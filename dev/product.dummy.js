export default [{
    title: "Nescafé Classic Coffee, 200G Dawn Jar",
    price: {
        regular: 540,
        sale: 491,
    },
    brand: {
        name: "Nescafé"
    },
    available: true,
    image: [
        "https://images-na.ssl-images-amazon.com/images/I/71zxqbC1a-L._SX679_.jpg",
        "https://images-na.ssl-images-amazon.com/images/I/710lQd6SnvL._SL1500_.jpg",
        "https://images-na.ssl-images-amazon.com/images/I/A1JVySKGpqL._SL1500_.jpg",
        "https://images-na.ssl-images-amazon.com/images/I/71yrm0F8KwL._SL1500_.jpg",
    ],
    info: "Item Weight: 200 g, \nItem Weight: 200 g, \nItem Weight: 200 g, \nItem Weight: 200 g, \nItem Weight: 200 g,",
    description: "To take your coffee experiences to the next level, NESCAFÉ, the world’s favourite instant coffee brand, brings forth a " +
        "rich and aromatic coffee in the form of NESCAFÉ Classic. The unmistakable flavour of NESCAFÉ Classic is what makes this signature " +
        "coffee so loved all over the world. Start your day right with the first sip of this classic 100% pure coffee and let the intense " +
        "taste and wonderfully refreshing aroma of NESCAFÉ instant coffee awaken your senses to new opportunities. With over 75 years of " +
        "experience and working with coffee farmers, to help them grow more sustainable coffee through improved crop techniques, " +
        "we deliver the best coffee produced by the best selecting, roasting and blending methods. Usage: Simply follow the instructions " +
        "on the back of the pack and mix the coffee powder with hot water or milk to get your own cup of premium instant coffee right at" +
        " home. The fine coffee powder creates rich, frothy instant coffee that will make a coffee lover smile with sheer delight. " +
        "Storage Recommendation: Do store the product in a cool, dry and hygienic place. To ensure lasting freshness, close the" +
        " lid tightly after every use. Always use a dry spoon."
}, {
    title: "Tata Tea Gold, 1kg",
    price: {
        regular: 457,
        sale: 420,
    },
    image: ["https://images-na.ssl-images-amazon.com/images/I/71GK6TEemKL._SL1500_.jpg"],
}, {
    title: "Nestle Everyday Dairy Whitener, Milk Powder for Tea, 1Kg Pouch",
    price: {
        regular: 490,
        sale: 0,
    },
    image: ["https://images-na.ssl-images-amazon.com/images/I/7181KOQX05L._SL1500_.jpg"],
}, {
    title: "Kellogg's Muesli with 21% Fruit and Nut, 750g",
    price: {
        regular: 415,
        sale: 414,
    },
    image: ["https://images-na.ssl-images-amazon.com/images/I/51QD3i7-wTL.jpg"],
}, {
    title: "Kissan Fresh Tomato Ketchup, 950g\n",
    price: {
        regular: 125,
        sale: 115,
    },
    image: ["https://images-na.ssl-images-amazon.com/images/I/71BW9IQg-%2BL._SL1000_.jpg"],
}, {
    title: "Nestle Everyday Dairy Whitener, Milk Powder for Tea, 1Kg Pouch",
    price: {
        regular: 490,
        sale: 0,
    },
    image: ["https://images-na.ssl-images-amazon.com/images/I/7181KOQX05L._SL1500_.jpg"],
}, {
    title: "Kellogg's Muesli with 21% Fruit and Nut, 750g",
    price: {
        regular: 415,
        sale: 414,
    },
    image: ["https://images-na.ssl-images-amazon.com/images/I/51QD3i7-wTL.jpg"],
}, {
    title: "Kissan Fresh Tomato Ketchup, 950g\n",
    price: {
        regular: 125,
        sale: 115,
    },
    image: ["https://images-na.ssl-images-amazon.com/images/I/71BW9IQg-%2BL._SL1000_.jpg"],
}]

