import Constants from "../constants";

const initialState = {
    login: {
        loading: false,
        data: {},
        error: {},
    },
    register: {
        loading: false,
        data: {},
        error: {},
    },
    verification: {
        loading: false,
        data: {},
        error: {},
    },
    checkToken: {
        loading: false,
        data: {},
        error: {},
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.AUTH_LOGIN_USER:
            return {
                ...state,
                login: {
                    loading: true,
                    data: {},
                    error: {},
                },
            };
        case Constants.AUTH_LOGIN_USER_SUCCESS:
            return {
                ...state,
                login: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.AUTH_LOGIN_USER_ERROR:
            return {
                ...state,
                login: {
                    loading: false,
                    data: {},
                    error: action.payload,
                },
            };
        case Constants.AUTH_REGISTER_USER:
            return {
                ...state,
                register: {
                    loading: true,
                    data: {},
                    error: {},
                },
            };
        case Constants.AUTH_REGISTER_USER_SUCCESS:
            return {
                ...state,
                register: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.AUTH_REGISTER_USER_ERROR:
            return {
                ...state,
                register: {
                    loading: false,
                    data: {},
                    error: action.payload,
                },
            };
        case Constants.AUTH_VERIFICATION_CODE:
            return {
                ...state,
                verification: {
                    loading: true,
                    data: {},
                    error: {},
                },
            };
        case Constants.AUTH_VERIFICATION_CODE_SUCCESS:
            return {
                ...state,
                verification: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.AUTH_VERIFICATION_CODE_ERROR:
            return {
                ...state,
                verification: {
                    loading: false,
                    data: {},
                    error: action.payload,
                },
            };
        case Constants.AUTH_CHECK_TOKEN:
            return {
                ...state,
                checkToken: {
                    loading: true,
                    data: {},
                    error: {},
                },
            };
        case Constants.AUTH_CHECK_TOKEN_SUCCESS:
            return {
                ...state,
                checkToken: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.AUTH_CHECK_TOKEN_ERROR:
            return {
                ...state,
                checkToken: {
                    loading: false,
                    data: {},
                    error: action.payload,
                },
            };
        default: {
            return state;
        }
    }
};
