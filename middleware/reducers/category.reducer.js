import Constants from "../constants";

const initialState = {
    all: {
        loading: false,
        data: [],
        error: {},
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.CATEGORY_GET_ALL:
            return {
                ...state,
                all: {
                    loading: true,
                    data: [],
                    error: {},
                }
            };
        case Constants.CATEGORY_GET_ALL_SUCCESS:
            return {
                ...state,
                all: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case Constants.CATEGORY_GET_ALL_ERROR:
            return {
                ...state,
                all: {
                    loading: false,
                    data: [],
                    error: action.payload,
                }
            };
        default: {
            return state;
        }
    }
};
