import {combineReducers} from 'redux';
import productReducer from "./product.reducer";
import cartReducer from "./cart.reducer";
import productHomeReducer from "./product.home.reducer";
import seeAllReducer from "./seeAll.reducer";
import categoryReducer from "./category.reducer";
import profileReducer from "./profile.reducer";
import authReducer from "./auth.reducer";
import orderReducer from "./order.reducer";
import addressReducer from "./address.reducer";
import checkoutReducer from "./checkout.reducer";

const rootReducer = combineReducers({
    product: productReducer,
    cart: cartReducer,
    product_home: productHomeReducer,
    seeAll: seeAllReducer,
    category: categoryReducer,
    profile: profileReducer,
    auth: authReducer,
    order: orderReducer,
    address: addressReducer,
    checkout: checkoutReducer,
});

export default rootReducer;
