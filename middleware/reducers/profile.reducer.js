import Constants from "../constants";

const initialState = {
    token: "",
    details: {},
    password: {
        loading: false,
        data: {},
        error: {},
    },
    email: {
        loading: false,
        data: {},
        error: {},
    },
    mobile: {
        loading: false,
        data: {},
        error: {},
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.PROFILE_SET_DETAILS:
            return {
                ...state,
                details: action.payload,
            };
        case Constants.PROFILE_UNSET_DETAILS:
            return {
                ...state,
                details: {},
            };
        case Constants.PROFILE_CHANGE_PASSWORD:
            return {
                ...state,
                password: {
                    loading: true,
                    data: {},
                    error: {},
                },
            };
        case Constants.PROFILE_CHANGE_PASSWORD_SUCCESS:
            return {
                ...state,
                password: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.PROFILE_CHANGE_PASSWORD_ERROR:
            return {
                ...state,
                password: {
                    loading: false,
                    data: {},
                    error: action.payload,
                },
            };
        case Constants.PROFILE_CHANGE_EMAIL:
            return {
                ...state,
                email: {
                    loading: true,
                    data: {},
                    error: {},
                },
            };
        case Constants.PROFILE_CHANGE_EMAIL_SUCCESS:
            return {
                ...state,
                email: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.PROFILE_CHANGE_EMAIL_ERROR:
            return {
                ...state,
                email: {
                    loading: false,
                    data: {},
                    error: action.payload,
                },
            };
        case Constants.PROFILE_CHANGE_MOBILE:
            return {
                ...state,
                mobile: {
                    loading: true,
                    data: {},
                    error: {},
                },
            };
        case Constants.PROFILE_CHANGE_MOBILE_SUCCESS:
            return {
                ...state,
                mobile: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.PROFILE_CHANGE_MOBILE_ERROR:
            return {
                ...state,
                mobile: {
                    loading: false,
                    data: {},
                    error: action.payload,
                },
            };
        case Constants.PROFILE_SET_TOKEN_SUCCESS:
            return {
                ...state,
                token: action.payload
            };
        case Constants.PROFILE_GET_TOKEN_SUCCESS:
            return {
                ...state,
                token: action.payload
            };
        default: {
            return state;
        }
    }
};
