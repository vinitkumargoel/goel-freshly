import Constants from "../constants";

const initialState = {
    all: {
        loading: false,
        data: [],
        error: {},
    },
    create: {
        loading: false,
        data: {},
        error: {},
    },
    remove: {
        loading: false,
        data: {},
        error: {},
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.ADDRESS_GET_ALL:
            return {
                ...state,
                all: {
                    loading: true,
                    data: [],
                    error: {},
                },
            };
        case Constants.ADDRESS_GET_ALL_SUCCESS:
            return {
                ...state,
                all: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.ADDRESS_GET_ALL_ERROR:
            return {
                ...state,
                all: {
                    loading: false,
                    data: [],
                    error: action.payload,
                },
            };

        case Constants.ADDRESS_CREATE:
            return {
                ...state,
                create: {
                    loading: true,
                    data: {},
                    error: {},
                },
            };
        case Constants.ADDRESS_CREATE_SUCCESS:
            return {
                ...state,
                create: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.ADDRESS_CREATE_ERROR:
            return {
                ...state,
                create: {
                    loading: false,
                    data: {},
                    error: action.payload,
                },
            };
        case Constants.ADDRESS_REMOVE:
            return {
                ...state,
                remove: {
                    loading: true,
                    data: {},
                    error: {},
                },
            };
        case Constants.ADDRESS_REMOVE_SUCCESS:
            return {
                ...state,
                remove: {
                    loading: false,
                    data: action.payload,
                    error: {},
                },
            };
        case Constants.ADDRESS_REMOVE_ERROR:
            return {
                ...state,
                remove: {
                    loading: false,
                    data: {},
                    error: action.payload,
                },
            };
        default: {
            return state;
        }
    }
};
