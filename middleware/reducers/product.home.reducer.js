import Constants from "../constants";

const initialState = {
    popular_products: {
        loading: false,
        data: [],
        error: {},
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.GET_POPULAR_PRODUCT_DETAILS:
            return {
                ...state,
                popular_products: {
                    loading: true,
                    data: [],
                    error: {},
                }
            };
        case Constants.GET_POPULAR_PRODUCT_DETAILS_SUCCESS:
            return {
                ...state,
                popular_products: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case Constants.GET_POPULAR_PRODUCT_DETAILS_ERROR:
            return {
                ...state,
                popular_products: {
                    loading: false,
                    data: [],
                    error: action.payload,
                }
            };
        default: {
            return state;
        }
    }
};
