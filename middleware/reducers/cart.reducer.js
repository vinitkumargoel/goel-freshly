import Constants from "../constants";

const initialState = {
    products: {
        loading: false,
        data: [],
        error: {},
    },
    detailedProduct: {
        loading: false,
        data: [],
        error: {},
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.GET_ALL_CART_DETAILED_PRODUCTS:
            return {
                ...state,
                detailedProduct: {
                    loading: true,
                    data: [],
                    error: {},
                }
            };
        case Constants.GET_ALL_CART_DETAILED_PRODUCTS_SUCCESS:
            return {
                ...state,
                detailedProduct: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case Constants.GET_ALL_CART_DETAILED_PRODUCTS_ERROR:
            return {
                ...state,
                detailedProduct: {
                    loading: false,
                    data: [],
                    error: action.payload,
                }
            };

        case Constants.CART_RESET:
            return {
                ...state,
                detailedProduct: {
                    loading: false,
                    data: [],
                    error: {},
                }
            };
        case Constants.CART_REFRESH_PRODUCT:
            return {
                ...state,
                detailedProduct: {
                    loading: true,
                    data: [],
                    error: {},
                }
            };
        case Constants.CART_REFRESH_PRODUCT_SUCCESS:
            return {
                ...state,
                detailedProduct: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case Constants.CART_REFRESH_PRODUCT_ERROR:
            return {
                ...state,
                detailedProduct: {
                    loading: false,
                    data: [],
                    error: action.payload,
                }
            };
        default: {
            return state;
        }
    }
};
