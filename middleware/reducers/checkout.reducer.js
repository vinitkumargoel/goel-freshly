import Constants from "../constants";

const initialState = {
    address: {},
    amount: 0,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.CHECKOUT_SET_ADDRESS:
            return {
                ...state,
                address: action.payload
            };
        case Constants.CHECKOUT_SET_AMOUNT:
            return {
                ...state,
                amount: action.payload
            };
        default: {
            return state;
        }
    }
};
