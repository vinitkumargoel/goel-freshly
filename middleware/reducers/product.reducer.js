import Constants from "../constants";

const initialState = {
    details: {
        loading: false,
        data: {},
        error: {},
    },
    seeAll: {
        loading: false,
        data: [],
        count: 0,
        error: {},
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.GET_PRODUCT_DETAILS:
            return {
                ...state,
                details: {
                    loading: true,
                    data: {},
                    error: {},
                }
            };
        case Constants.GET_PRODUCT_DETAILS_SUCCESS:
            return {
                ...state,
                details: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case Constants.GET_PRODUCT_DETAILS_ERROR:
            return {
                ...state,
                details: {
                    loading: false,
                    data: [],
                    error: action.payload,
                }
            };
        case Constants.GET_PRODUCT_SEE_ALL:
            return {
                ...state,
                seeAll: {
                    loading: true,
                    data: [],
                    count: 0,
                    error: {},
                }
            };
        case Constants.GET_PRODUCT_SEE_ALL_SUCCESS:
            return {
                ...state,
                seeAll: {
                    loading: false,
                    data: action.payload.data,
                    count: action.payload.count,
                    error: {},
                }
            };
        case Constants.GET_PRODUCT_SEE_ALL_ERROR:
            return {
                ...state,
                seeAll: {
                    loading: false,
                    data: [],
                    count: 0,
                    error: action.payload,
                }
            };
        default: {
            return state;
        }
    }
};
