import Constants from "../constants";

const initialState = {
    filters: {
        query: "",
        limit: 10,
        category_l1: {},
        category_l2: {},
        sort: "POPULARITY",
        page_number: 1,
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.SEE_ALL_CHANGE_QUERY:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    category_l1: {},
                    category_l2: {},
                    sort: "POPULARITY",
                    query: action.payload,
                    page_number: 1,
                }
            };
        case Constants.SEE_ALL_CHANGE_PAGE_NUMBER:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    page_number: action.payload,
                }
            };
        case Constants.SEE_ALL_CHANGE_SORT:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    sort: action.payload,
                    page_number: 1,
                }
            };
        case Constants.SEE_ALL_CHANGE_CATEGORY_L1:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    category_l1: action.payload || {},
                    page_number: 1,
                }
            };
        case Constants.SEE_ALL_CHANGE_CATEGORY_L2:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    category_l2: action.payload || {},
                    page_number: 1,
                }
            };
        case Constants.SEE_ALL_CHANGE_CATEGORY:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    category_l1: action.payload.l1 || {},
                    category_l2: action.payload.l2 || {},
                    page_number: 1,
                }
            };
        case Constants.SEE_ALL_CLEAR_FILTERS:
            return {
                ...state,
                filters: {
                    ...state.filters,
                    query: "",
                    category_l1: {},
                    category_l2: {},
                    sort: "POPULARITY",
                    page_number: 1,
                }
            };
        default: {
            return state;
        }
    }
};
