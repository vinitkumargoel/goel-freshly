import Constants from "../constants";

const initialState = {
    details: {
        loading: false,
        data: {},
        error: {},
    },
    all: {
        loading: false,
        data: [],
        error: {},
    },
    create: {
        loading: false,
        data: {},
        error: {},
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.ORDER_GET_ALL:
            return {
                ...state,
                all: {
                    loading: true,
                    data: [],
                    error: {},
                }
            };
        case Constants.ORDER_GET_ALL_SUCCESS:
            return {
                ...state,
                all: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case Constants.ORDER_GET_ALL_ERROR:
            return {
                ...state,
                all: {
                    loading: false,
                    data: [],
                    error: action.payload,
                }
            };
        case Constants.ORDER_GET_DETAILS:
            return {
                ...state,
                details: {
                    loading: true,
                    data: {},
                    error: {},
                }
            };
        case Constants.ORDER_GET_DETAILS_SUCCESS:
            return {
                ...state,
                details: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case Constants.ORDER_GET_DETAILS_ERROR:
            return {
                ...state,
                details: {
                    loading: false,
                    data: {},
                    error: action.payload,
                }
            };
        case Constants.ORDER_CREATE:
            return {
                ...state,
                create: {
                    loading: true,
                    data: {},
                    error: {},
                }
            };
        case Constants.ORDER_CREATE_SUCCESS:
            return {
                ...state,
                create: {
                    loading: false,
                    data: action.payload,
                    error: {},
                }
            };
        case Constants.ORDER_CREATE_ERROR:
            return {
                ...state,
                create: {
                    loading: false,
                    data: {},
                    error: action.payload,
                }
            };
        case Constants.ORDER_CREATE_RESET:
            return {
                ...state,
                create: {
                    loading: false,
                    data: {},
                    error: {},
                }
            };
        default: {
            return state;
        }
    }
};
