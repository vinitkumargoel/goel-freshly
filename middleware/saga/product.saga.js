import axios from "axios";
import {put, takeLatest} from 'redux-saga/effects';
import Constants from "../constants";
import {base_url, getProductById, productsSeeAll} from "../../utils/api.constants";

function* getProductDetails(action) {
    try {
        let res = yield axios.get(`${base_url}${getProductById}?id=${action.payload.id}`, {
            headers: {
                authorization: `Bearer ${action.payload.token}`
            }
        });
        yield put({
            type: Constants.GET_PRODUCT_DETAILS_SUCCESS,
            payload: res.data.data,
        });
    } catch (error) {
        yield put({
            type: Constants.GET_PRODUCT_DETAILS_ERROR,
            payload: error
        });
    }
}

export function* getProductDetailsWatch() {
    yield takeLatest(Constants.GET_PRODUCT_DETAILS, getProductDetails);
}

function* getSeeAllProduct(action) {
    try {
        let res = yield axios.post(`${base_url}${productsSeeAll}`, action.payload.body, {
            headers: {
                authorization: `Bearer ${action.payload.token}`
            }
        });
        yield put({
            type: Constants.GET_PRODUCT_SEE_ALL_SUCCESS,
            payload: res.data,
        });
    } catch (error) {
        yield put({
            type: Constants.GET_PRODUCT_SEE_ALL_ERROR,
            payload: error
        });
    }
}


export function* getSeeAllProductWatch() {
    yield takeLatest(Constants.GET_PRODUCT_SEE_ALL, getSeeAllProduct);
}
