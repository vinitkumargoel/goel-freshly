import {all, fork} from 'redux-saga/effects';
import {getProductDetailsWatch, getSeeAllProductWatch} from './product.saga';
import {
    addProductWatch,
    changeQuantityWatch,
    getAllDetailedProductInCartWatch,
    removeProductWatch,
    refreshProductsWatch,
    resetCartWatch
} from './cart.saga';
import {getPopularProductsWatch} from "./product.home.saga";
import {getAllCategoryWatch} from "./category.saga";
import {loginUserWatch, registerUserWatch, verificationCodeWatch, checkTokenWatch, logoutUserWatch} from "./auth.saga";
import {getAllOrdersWatch, getSingleOrderDetailsWatch, createOrderWatch} from "./order.saga";
import {createAddressWatch, getAllAddressWatch, removeAddressWatch} from "./address.saga";
import {changeEmailWatch, changeMobileWatch, changePasswordWatch, getTokenWatch, setTokenWatch} from "./profile.saga";

export default function* rootSaga() {
    yield all([
        fork(getProductDetailsWatch),
        fork(getSeeAllProductWatch),
        fork(addProductWatch),
        fork(removeProductWatch),
        fork(refreshProductsWatch),
        fork(resetCartWatch),
        fork(getAllDetailedProductInCartWatch),
        fork(changeQuantityWatch),
        fork(getPopularProductsWatch),
        fork(getAllCategoryWatch),
        fork(loginUserWatch),
        fork(registerUserWatch),
        fork(verificationCodeWatch),
        fork(getAllOrdersWatch),
        fork(getSingleOrderDetailsWatch),
        fork(createOrderWatch),
        fork(createAddressWatch),
        fork(getAllAddressWatch),
        fork(removeAddressWatch),
        fork(changePasswordWatch),
        fork(changeMobileWatch),
        fork(changeEmailWatch),
        fork(getTokenWatch),
        fork(setTokenWatch),
        fork(checkTokenWatch),
        fork(logoutUserWatch),
    ])
};
