import axios from "axios";
import {put, takeLatest} from 'redux-saga/effects';
import Constants from "../constants";
import {base_url, createOrderApi, getByIdOrderApi, listOrderApi} from "../../utils/api.constants";

function* getAllOrders(action) {
    try {
        let res = yield axios.get(`${base_url}${listOrderApi}`, {
            headers: {authorization: `Bearer ${action.payload.token}`}
        });
        yield put({
            type: Constants.ORDER_GET_ALL_SUCCESS,
            payload: res.data.data,
        });
    } catch (error) {
        yield put({
            type: Constants.ORDER_GET_ALL_ERROR,
            payload: error,
        });
    }
}

export function* getAllOrdersWatch() {
    yield takeLatest(Constants.ORDER_GET_ALL, getAllOrders);
}


function* getSingleOrderDetails(action) {
    try {
        let res = yield axios.get(`${base_url}${getByIdOrderApi}?id=${action.payload.id}`, {
            headers: {authorization: `Bearer ${action.payload.token}`}
        });
        yield put({
            type: Constants.ORDER_GET_DETAILS_SUCCESS,
            payload: res.data,
        });
    } catch (error) {
        yield put({
            type: Constants.ORDER_GET_DETAILS_ERROR,
            payload: error,
        });
    }
}

export function* getSingleOrderDetailsWatch() {
    yield takeLatest(Constants.ORDER_GET_DETAILS, getSingleOrderDetails);
}

function* createOrder(action) {
    try {
        let res = yield axios.post(`${base_url}${createOrderApi}`, action.payload.body, {
            headers: {authorization: `Bearer ${action.payload.token}`}
        });
        yield put({
            type: Constants.ORDER_CREATE_SUCCESS,
            payload: res.data,
        });
    } catch (error) {
        yield put({
            type: Constants.ORDER_CREATE_ERROR,
            payload: error,
        });
    }
}

export function* createOrderWatch() {
    yield takeLatest(Constants.ORDER_CREATE, createOrder);
}
