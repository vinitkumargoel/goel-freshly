import {put, takeLatest, takeEvery, take} from 'redux-saga/effects';
import Constants from "../constants";
import axios from "axios";
import {base_url, checkTokenAPI, loginUserAPI, registerUserAPI} from "../../utils/api.constants";

function* loginUser(action) {
    try {
        let res = yield axios.post(`${base_url}${loginUserAPI}`, action.payload);
        if (res.data.status) {
            yield put({type: Constants.AUTH_LOGIN_USER_SUCCESS, payload: res.data})
        } else {
            yield put({type: Constants.AUTH_LOGIN_USER_ERROR, payload: res.data})
        }
    } catch (error) {
        yield put({type: Constants.AUTH_LOGIN_USER_ERROR, payload: error,})
    }
}

export function* loginUserWatch() {
    yield takeLatest(Constants.AUTH_LOGIN_USER, loginUser);
}

function* registerUser(action) {
    try {
        let res = yield axios.post(`${base_url}${registerUserAPI}`, action.payload);
        yield put({type: Constants.AUTH_REGISTER_USER_SUCCESS, payload: res.data});
    } catch (error) {
        yield put({type: Constants.AUTH_REGISTER_USER_ERROR, payload: error});
    }
}

export function* registerUserWatch() {
    yield takeLatest(Constants.AUTH_REGISTER_USER, registerUser);
}


function* verificationCode(action) {
    try {
        yield put({type: Constants.AUTH_VERIFICATION_CODE_SUCCESS, payload: {},})
    } catch (error) {
        yield put({type: Constants.AUTH_VERIFICATION_CODE_ERROR, payload: error,})
    }
}

export function* verificationCodeWatch() {
    yield takeLatest(Constants.AUTH_VERIFICATION_CODE, verificationCode);
}

function* checkToken(action) {
    try {
        let res = yield axios.post(`${base_url}${checkTokenAPI}`, action.payload);
        if (res.data.status) {
            yield put({type: Constants.AUTH_CHECK_TOKEN_SUCCESS, payload: res.data});
            yield put({type: Constants.PROFILE_SET_DETAILS, payload: res.data.user});
        } else {
            yield put({type: Constants.AUTH_CHECK_TOKEN_ERROR, payload: res.data})
        }
    } catch (error) {
        console.log(error);
        yield put({type: Constants.AUTH_CHECK_TOKEN_ERROR, payload: error})
    }
}

export function* checkTokenWatch() {
    yield takeLatest(Constants.AUTH_CHECK_TOKEN, checkToken);
}


function* logoutUser(action) {
    try {
        yield put({type: Constants.PROFILE_SET_TOKEN, payload: ""});
        yield put({type: Constants.PROFILE_SET_DETAILS, payload: {}});
    } catch (error) {
        console.log(error);
        yield put({type: Constants.AUTH_LOGOUT_ERROR, payload: error})
    }
}

export function* logoutUserWatch() {
    yield takeLatest(Constants.AUTH_LOGOUT, logoutUser);
}
