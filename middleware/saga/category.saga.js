import axios from "axios";
import {put, takeLatest} from 'redux-saga/effects';
import Constants from "../constants";
import {base_url, categoryAll} from "../../utils/api.constants";

function* getAllCategory() {
    try {
        let res = yield axios.get(`${base_url}${categoryAll}`);
        yield put({
            type: Constants.CATEGORY_GET_ALL_SUCCESS,
            payload: res.data.data,
        });
    } catch (error) {
        yield put({
            type: Constants.CATEGORY_GET_ALL_ERROR,
            payload: error
        });
    }
}

export function* getAllCategoryWatch() {
    yield takeLatest(Constants.CATEGORY_GET_ALL, getAllCategory);
}
