import axios from "axios";
import {put, takeLatest} from 'redux-saga/effects';
import Constants from "../constants";
import {base_url, productsPopular} from "../../utils/api.constants";

function* getPopularProducts(action) {
    try {
        let res = yield axios.get(`${base_url}${productsPopular}?limit=${action.payload.limit}`, {
            headers: {
                authorization: `Bearer ${action.payload.token}`
            }
        });
        yield put({
            type: Constants.GET_POPULAR_PRODUCT_DETAILS_SUCCESS,
            payload: res.data.data,
        });
    } catch (error) {
        yield put({
            type: Constants.GET_POPULAR_PRODUCT_DETAILS_ERROR,
            payload: error
        });
    }
}

export function* getPopularProductsWatch() {
    yield takeLatest(Constants.GET_POPULAR_PRODUCT_DETAILS, getPopularProducts);
}
