// Imports: Dependencies
import {put, takeLatest} from 'redux-saga/effects';
import Constants from "../constants";
import {AsyncStorage} from 'react-native';
import axios from "axios";
import {base_url, getProductById} from "../../utils/api.constants";

function* addProduct(action) {
    try {
        let value = (yield AsyncStorage.getItem('CART')) || "[]";
        value = JSON.parse(value);
        value.push({id: action.payload._id, product: action.payload, quantity: 1,});
        yield AsyncStorage.setItem('CART', JSON.stringify(value));
        yield put({type: Constants.GET_ALL_CART_DETAILED_PRODUCTS_SUCCESS, payload: value || []});
    } catch (error) {
        yield put({type: Constants.GET_ALL_CART_DETAILED_PRODUCTS_ERROR, payload: error});
    }
}

function* removeProduct(action) {
    try {
        let value = (yield AsyncStorage.getItem('CART')) || "[]";
        value = JSON.parse(value);
        let final = [];
        for (let i of value) {
            if (i.id !== action.payload) final.push(i);
        }
        yield AsyncStorage.setItem('CART', JSON.stringify(final));
        yield put({type: Constants.GET_ALL_CART_DETAILED_PRODUCTS_SUCCESS, payload: final});
    } catch (error) {
        yield put({type: Constants.GET_ALL_CART_DETAILED_PRODUCTS_ERROR, payload: error});
    }
}

function* getAllDetailedProductInCart() {
    try {
        let value = (yield AsyncStorage.getItem('CART')) || "[]";
        yield put({type: Constants.GET_ALL_CART_DETAILED_PRODUCTS_SUCCESS, payload: JSON.parse(value)});
    } catch (error) {
        yield put({type: Constants.GET_ALL_CART_DETAILED_PRODUCTS_ERROR, payload: error});
    }
}

function* changeQuantity(action) {
    try {
        let value = (yield AsyncStorage.getItem('CART')) || "[]";
        value = JSON.parse(value);
        let final = [];
        for (let i of value) {
            if (i.id === action.payload.id) final.push({...i, quantity: action.payload.quantity});
            else final.push(i);
        }
        yield AsyncStorage.setItem('CART', JSON.stringify(final));
        yield put({type: Constants.GET_ALL_CART_DETAILED_PRODUCTS_SUCCESS, payload: final});
    } catch (error) {
        yield put({type: Constants.GET_ALL_CART_DETAILED_PRODUCTS_ERROR, payload: error});
    }
}

function* refreshProducts(action) {
    try {
        let value = (yield AsyncStorage.getItem('CART')) || "[]";
        let final = [];
        value = JSON.parse(value);
        for (let i of value) {
            let res = yield axios.get(`${base_url}${getProductById}?id=${i.id}`, {
                headers: {
                    authorization: `Bearer ${action.payload.token}`
                }
            });
            final.push({...i, product: res.data.data});
        }
        yield AsyncStorage.setItem('CART', JSON.stringify(final));
        yield put({type: Constants.CART_REFRESH_PRODUCT_SUCCESS, payload: final});
    } catch (error) {
        console.error(error)
        yield put({type: Constants.CART_REFRESH_PRODUCT_ERROR, payload: error});
    }
}

function* resetCart(action) {
    try {
        yield AsyncStorage.setItem('CART', JSON.stringify([]));
        yield put({type: Constants.CART_REFRESH_PRODUCT_SUCCESS, payload: []});
    } catch (error) {
        console.error(error);
        yield put({type: Constants.CART_REFRESH_PRODUCT_ERROR, payload: error});
    }
}

export function* addProductWatch() {
    yield takeLatest(Constants.ADD_IN_CART, addProduct);
}

export function* removeProductWatch() {
    yield takeLatest(Constants.REMOVE_FROM_CART, removeProduct);
}

export function* getAllDetailedProductInCartWatch() {
    yield takeLatest(Constants.GET_ALL_CART_DETAILED_PRODUCTS, getAllDetailedProductInCart);
}

export function* changeQuantityWatch() {
    yield takeLatest(Constants.CHANGE_PRODUCT_QUANTITY, changeQuantity);
}

export function* refreshProductsWatch() {
    yield takeLatest(Constants.CART_REFRESH_PRODUCT, refreshProducts);
}


export function* resetCartWatch() {
    yield takeLatest(Constants.CART_RESET, resetCart);
}
