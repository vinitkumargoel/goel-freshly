import axios from "axios";
import {put, takeLatest} from 'redux-saga/effects';
import Constants from "../constants";
import {base_url, categoryAll, createAddressApi, getAllAddressApi, removeAddressApi} from "../../utils/api.constants";

function* createAddress(action) {
    try {
        let res = yield axios.post(`${base_url}${createAddressApi}`, action.payload.body, {
            headers: {
                authorization: `Bearer ${action.payload.token}`
            }
        });
        yield put({
            type: Constants.ADDRESS_CREATE_SUCCESS,
            payload: res.data.data,
        });
    } catch (error) {
        yield put({
            type: Constants.ADDRESS_CREATE_ERROR,
            payload: error
        });
    }
}

export function* createAddressWatch() {
    yield takeLatest(Constants.ADDRESS_CREATE, createAddress);
}


function* getAllAddress(action) {
    try {
        let res = yield axios.get(`${base_url}${getAllAddressApi}`, {
            headers: {
                authorization: `Bearer ${action.payload.token}`
            }
        });
        yield put({
            type: Constants.ADDRESS_GET_ALL_SUCCESS,
            payload: res.data.data,
        });
    } catch (error) {
        yield put({
            type: Constants.ADDRESS_GET_ALL_ERROR,
            payload: error
        });
    }
}

export function* getAllAddressWatch() {
    yield takeLatest(Constants.ADDRESS_GET_ALL, getAllAddress);
}

function* removeAddress(action) {
    try {
        let res = yield axios.get(`${base_url}${removeAddressApi}?id=${action.payload.id}`, {
            headers: {
                authorization: `Bearer ${action.payload.token}`
            }
        });
        yield put({
            type: Constants.ADDRESS_REMOVE_SUCCESS,
            payload: res.data.data,
        });
    } catch (error) {
        yield put({
            type: Constants.ADDRESS_REMOVE_ERROR,
            payload: error
        });
    }
}

export function* removeAddressWatch() {
    yield takeLatest(Constants.ADDRESS_REMOVE, removeAddress);
}
