// Imports: Dependencies
import {put, takeLatest} from 'redux-saga/effects';
import Constants from "../constants";
import {AsyncStorage} from 'react-native';

function* changePassword(action) {
    try {
        yield put({
            type: Constants.PROFILE_CHANGE_PASSWORD_SUCCESS,
            payload: {}
        });
    } catch (error) {
        yield put({
            type: Constants.PROFILE_CHANGE_PASSWORD_ERROR,
            payload: error
        });
    }
}

export function* changePasswordWatch() {
    yield takeLatest(Constants.PROFILE_CHANGE_PASSWORD, changePassword);
}

function* changeEmail(action) {
    try {
        yield put({
            type: Constants.PROFILE_CHANGE_EMAIL_SUCCESS,
            payload: {}
        });
    } catch (error) {
        yield put({
            type: Constants.PROFILE_CHANGE_EMAIL_ERROR,
            payload: error
        });
    }
}

export function* changeEmailWatch() {
    yield takeLatest(Constants.PROFILE_CHANGE_EMAIL, changeEmail);
}

function* changeMobile(action) {
    try {
        yield put({
            type: Constants.PROFILE_CHANGE_MOBILE_SUCCESS,
            payload: {}
        });
    } catch (error) {
        yield put({
            type: Constants.PROFILE_CHANGE_MOBILE_ERROR,
            payload: error
        });
    }
}

export function* changeMobileWatch() {
    yield takeLatest(Constants.PROFILE_CHANGE_MOBILE, changeMobile);
}


function* setToken(action) {
    try {
        yield AsyncStorage.setItem('TOKEN', action.payload);
        yield put({
            type: Constants.PROFILE_SET_TOKEN_SUCCESS,
            payload: action.payload
        });
    } catch (error) {
        yield put({
            type: Constants.PROFILE_SET_TOKEN_ERROR,
            payload: error
        });
    }
}

export function* setTokenWatch() {
    yield takeLatest(Constants.PROFILE_SET_TOKEN, setToken);
}

function* getToken(action) {
    try {
        let token = yield AsyncStorage.getItem('TOKEN');
        yield put({
            type: Constants.PROFILE_GET_TOKEN_SUCCESS,
            payload: token
        });
    } catch (error) {
        yield put({
            type: Constants.PROFILE_GET_TOKEN_ERROR,
            payload: error
        });
    }
}

export function* getTokenWatch() {
    yield takeLatest(Constants.PROFILE_GET_TOKEN, getToken);
}
