export const SEE_ALL_CHANGE_QUERY = "product/see-all/filter/change-query";
export const SEE_ALL_CHANGE_SORT = "product/see-all/filter/change-sort";
export const SEE_ALL_CHANGE_CATEGORY_L1 = "product/see-all/filter/change-category/l1";
export const SEE_ALL_CHANGE_CATEGORY_L2 = "product/see-all/filter/change-category/l2";
export const SEE_ALL_CHANGE_CATEGORY = "product/see-all/filter/change-category";
export const SEE_ALL_CLEAR_FILTERS = "product/see-all/filter/clear-filters";

export const SEE_ALL_CHANGE_PAGE_NUMBER = "product/see-all/filter/change-page-number";
export const SEE_ALL_CHANGE_PAGE_NUMBER_SUCCESS = "product/see-all/filter/change-page-number-success";
export const SEE_ALL_CHANGE_PAGE_NUMBER_ERROR = "product/see-all/filter/change-page-number-error";
