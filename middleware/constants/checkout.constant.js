export const CHECKOUT_SET_AMOUNT = "checkout/set-amount";
export const CHECKOUT_SET_ADDRESS = "checkout/set-address";
export const CHECKOUT_SET_DATE = "checkout/set-date";
