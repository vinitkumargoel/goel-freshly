export const ORDER_GET_ALL = "order/get-all";
export const ORDER_GET_ALL_SUCCESS = "order/get-all-success";
export const ORDER_GET_ALL_ERROR = "order/get-all-error";

export const ORDER_GET_DETAILS = "order/get-details";
export const ORDER_GET_DETAILS_SUCCESS = "order/get-details-success";
export const ORDER_GET_DETAILS_ERROR = "order/get-details-error";

export const ORDER_CREATE = "order/create";
export const ORDER_CREATE_SUCCESS = "order/create-success";
export const ORDER_CREATE_ERROR = "order/create-error";

export const ORDER_CREATE_RESET = "order/create-reset";
