export const CATEGORY_GET_ALL = "category/get-all";
export const CATEGORY_GET_ALL_SUCCESS = "category/get-all-success";
export const CATEGORY_GET_ALL_ERROR = "category/get-all-error";
