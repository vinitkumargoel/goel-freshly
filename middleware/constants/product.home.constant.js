export const GET_POPULAR_PRODUCT_DETAILS = "home/get-popular-product-details";
export const GET_POPULAR_PRODUCT_DETAILS_SUCCESS = "home/get-popular-product-details/success";
export const GET_POPULAR_PRODUCT_DETAILS_ERROR = "home/get-popular-product-details/error";
