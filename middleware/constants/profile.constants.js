export const PROFILE_SET_DETAILS = "profile/set-details";
export const AUTH_LOGOUTPROFILE_UNSET_DETAILS = "profile/unset-details";

export const PROFILE_SET_TOKEN = "profile/set-token";
export const PROFILE_SET_TOKEN_SUCCESS = "profile/set-token-success";
export const PROFILE_SET_TOKEN_ERROR = "profile/set-token-error";

export const PROFILE_GET_TOKEN = "profile/get-token";
export const PROFILE_GET_TOKEN_SUCCESS = "profile/get-token-success";
export const PROFILE_GET_TOKEN_ERROR = "profile/get-token-error";

export const PROFILE_CHANGE_PASSWORD = "profile/change-password";
export const PROFILE_CHANGE_PASSWORD_SUCCESS = "profile/change-password-success";
export const PROFILE_CHANGE_PASSWORD_ERROR = "profile/change-password-error";

export const PROFILE_CHANGE_EMAIL = "profile/change-email";
export const PROFILE_CHANGE_EMAIL_SUCCESS = "profile/change-email-success";
export const PROFILE_CHANGE_EMAIL_ERROR = "profile/change-email-error";

export const PROFILE_CHANGE_MOBILE = "profile/change-mobile";
export const PROFILE_CHANGE_MOBILE_SUCCESS = "profile/change-mobile-success";
export const PROFILE_CHANGE_MOBILE_ERROR = "profile/change-mobile-error";
