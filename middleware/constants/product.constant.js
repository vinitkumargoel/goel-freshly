export const GET_PRODUCT_DETAILS = "product/get-product-details";
export const GET_PRODUCT_DETAILS_SUCCESS = "product/get-product-details/success";
export const GET_PRODUCT_DETAILS_ERROR = "product/get-product-details/error";

export const GET_PRODUCT_SEE_ALL = "product/get-product-see-all";
export const GET_PRODUCT_SEE_ALL_SUCCESS = "product/get-product-see-all/success";
export const GET_PRODUCT_SEE_ALL_ERROR = "product/get-product-see-all/error";
