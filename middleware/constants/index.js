import * as productConstant from "./product.constant";
import * as cartConstant from "./cart.constant";
import * as productHomeConstant from "./product.home.constant";
import * as seeAllConstant from "./seeAll.constant";
import * as categoryConstant from "./category.constant";
import * as profileConstant from "./profile.constants";
import * as authConstant from "./auth.constant";
import * as ordersConstant from "./orders.constant";
import * as addressConstant from "./address.constants";
import * as checkoutConstant from "./checkout.constant";

export default {
    ...productConstant,
    ...cartConstant,
    ...productHomeConstant,
    ...seeAllConstant,
    ...categoryConstant,
    ...profileConstant,
    ...authConstant,
    ...ordersConstant,
    ...addressConstant,
    ...checkoutConstant,
};
