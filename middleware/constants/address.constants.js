export const ADDRESS_GET_ALL = "address/get-all";
export const ADDRESS_GET_ALL_SUCCESS = "address/get-all-success";
export const ADDRESS_GET_ALL_ERROR = "address/get-all-error";

export const ADDRESS_CREATE = "address/create";
export const ADDRESS_CREATE_SUCCESS = "address/create-success";
export const ADDRESS_CREATE_ERROR = "address/create-error";

export const ADDRESS_REMOVE = "address/remove";
export const ADDRESS_REMOVE_SUCCESS = "address/remove-success";
export const ADDRESS_REMOVE_ERROR = "address/remove-error";
