export const AUTH_LOGIN_USER = "auth/login-user";
export const AUTH_LOGIN_USER_SUCCESS = "auth/login-user-success";
export const AUTH_LOGIN_USER_ERROR = "auth/login-user-error";

export const AUTH_REGISTER_USER = "auth/register-user";
export const AUTH_REGISTER_USER_SUCCESS = "auth/register-user-success";
export const AUTH_REGISTER_USER_ERROR = "auth/register-user-error";

export const AUTH_VERIFICATION_CODE = "auth/verification-code";
export const AUTH_VERIFICATION_CODE_SUCCESS = "auth/verification-code-success";
export const AUTH_VERIFICATION_CODE_ERROR = "auth/verification-code-error";

export const AUTH_CHECK_TOKEN = "auth/check-token";
export const AUTH_CHECK_TOKEN_SUCCESS = "auth/check-token-success";
export const AUTH_CHECK_TOKEN_ERROR = "auth/check-token-error";

export const AUTH_LOGOUT = "auth/logout";
export const AUTH_LOGOUT_SUCCESS = "auth/logout-success";
export const AUTH_LOGOUT_ERROR = "auth/logout-error";
