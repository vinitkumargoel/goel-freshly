export const GET_ALL_CART_PRODUCTS = "cart/product/get-all";
export const GET_ALL_CART_PRODUCTS_SUCCESS = "cart/product/get-all/success";
export const GET_ALL_CART_PRODUCTS_ERROR = "cart/product/get-all/error";

export const ADD_IN_CART = "cart/product/add";
export const ADD_IN_CART_SUCCESS = "cart/product/add/success";
export const ADD_IN_CART_ERROR = "cart/product/add/error";

export const REMOVE_FROM_CART = "cart/product/remove";
export const REMOVE_FROM_CART_SUCCESS = "cart/product/remove/success";
export const REMOVE_FROM_CART_ERROR = "cart/product/remove/error";

export const CHANGE_PRODUCT_QUANTITY = "cart/product/quantity/change";
export const CHANGE_PRODUCT_QUANTITY_SUCCESS = "cart/product/quantity/change/success";
export const CHANGE_PRODUCT_QUANTITY_ERROR = "cart/product/quantity/change/error";

export const GET_ALL_CART_DETAILED_PRODUCTS = "cart/detailed-product/get-all";
export const GET_ALL_CART_DETAILED_PRODUCTS_SUCCESS = "cart/detailed-product/get-all/success";
export const GET_ALL_CART_DETAILED_PRODUCTS_ERROR = "cart/detailed-product/get-all/error";

export const CART_REFRESH_PRODUCT = "cart/refresh-product";
export const CART_REFRESH_PRODUCT_SUCCESS = "cart/refresh-product-success";
export const CART_REFRESH_PRODUCT_ERROR = "cart/refresh-product-error";

export const CART_RESET = "cart/reset";
