export default {
    config: (profile, amount) => {
        return {
            description: 'Goel Freshly Payment.',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            // key: 'rzp_live_91vU8rDIw51TUj',
            key: 'rzp_test_1DP5mmOlF5G5ag',
            amount: amount * 100,
            name: profile.name,
            prefill: {
                email: profile.email,
                contact: profile.mobile,
                name: profile.name,
            },
            theme: {color: '#F37254'}
        }
    },
    finalOrderData: (checkoutData, cartData, paymentId) => {
        return {
            date: new Date(),
            address: checkoutData.address,
            amount: checkoutData.amount,
            products: cartData,
            razorpay_payment_id: paymentId,
        }
    },
}
