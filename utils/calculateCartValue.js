import utils from "../config/utils";
import {DELIVERY_PRICE} from "./config";

export default data => {
    let subTotal = data.reduce((a, b) => a + (b.product.price.regular * b.quantity), 0);
    let discount = data.reduce((a, b) => {
        let isSale = utils.isSaleProduct(b.product.price);
        if (isSale) {
            return a + (b.product.price.regular - b.product.price.sale) * b.quantity;
        } else {
            return a;
        }

    }, 0);
    let delivery = DELIVERY_PRICE;
    let totalPayable = (subTotal + delivery) - discount;
    return {subTotal, discount, delivery, totalPayable}
}
