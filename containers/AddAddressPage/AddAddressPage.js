import React, {useEffect, useState} from 'react';
import styles from "./styles";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import CustomTextBox from "../../components/CustomTextBox";
import CustomButton from "../../components/CustomButton";
import isEmpty from "lodash/isEmpty";
import AlertBox from "../../components/AlertBox";
import Constants from "../../middleware/constants";

const AddAddressPage = ({navigation, route, ...props}) => {
    let [state, setState] = useState({
        name: "",
        mobile: "",
        building: "",
        locality: "",
        landmark: "",
    });
    const onAddAddress = () => {
        if (isEmpty(state.name) ||
            isEmpty(state.mobile) ||
            isEmpty(state.building) ||
            isEmpty(state.locality) ||
            isEmpty(state.landmark)
        ) {
            AlertBox('Please Enter Details!')
        } else {
            props.createAddress(state, props.token);
        }
    };
    useEffect(() => {
        if (!isEmpty(props.createAPI.error)) AlertBox("Error while login!!");
        if (!isEmpty(props.createAPI.data)) {
            AlertBox("Successfully Added");
            navigation.goBack()
        }
    }, [props.createAPI]);
    return (
        <View style={styles.AddAddressPage}>
            <Header left={"BACK"} title={"Add New Address"} cart={false} search={false}/>
            <View style={{...styles.appContainer, ...styles.AddAddressPageContainer}}>
                <CustomTextBox
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    placeholder={'Full Name'}
                    value={state.name}
                    onChangeText={text => setState({...state, name: text})}
                />
                <CustomTextBox
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    keyboardType={"number-pad"}
                    maxLength={10}
                    placeholder={'Mobile Number without prefixes'}
                    value={state.mobile}
                    onChangeText={text => setState({...state, mobile: text})}
                />
                <CustomTextBox
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    placeholder={'Room and Building Name'}
                    value={state.building}
                    onChangeText={text => setState({...state, building: text})}
                />
                <CustomTextBox
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    placeholder={'Locality'}
                    value={state.locality}
                    onChangeText={text => setState({...state, locality: text})}
                />
                <CustomTextBox
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    placeholder={'Landmark'}
                    value={state.landmark}
                    onChangeText={text => setState({...state, landmark: text})}
                />
                <CustomButton type={'red'} onPress={onAddAddress}>Add Address</CustomButton>
            </View>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        token: state.profile.token || "",
        createAPI: state.address.create || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        createAddress: (body, token) => dispatch({type: Constants.ADDRESS_CREATE, payload: {body, token}}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddAddressPage);
