import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color} from "../../config/variables";

const styles = StyleSheet.create({
    ...CommonStyles,
    AddAddressPage: {
        backgroundColor: Color.white,
        height: '100%',
    },
    AddAddressPageContainer: {
        marginHorizontal: 20,
    },
    inputBox: {
        marginTop: 12,
    }
});

export default styles
