import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color, TOP_PADDING} from "../../config/variables";
import utils from "../../config/utils";

const styles = StyleSheet.create({
    ...CommonStyles,
    Header: {
        paddingTop: TOP_PADDING,
        paddingHorizontal: 20,
        flexDirection: 'row',
        height: 100,
        backgroundColor: Color.white,
    },
    left: {
        paddingTop: 10,
        color: Color.black,
        width: utils.columnSize * 2
    },
    center: {
        width: utils.columnSize * 19
    },
    right: {
        paddingTop: 10,
        marginRight: 20,
        alignItems: "center",
        width: utils.columnSize * 3
    },
    menuButton: {
        borderWidth: 0,
        padding: 0,
        height: 30,
        width: 30,
        // marginRight: 10,
    },
    searchBar: {
        marginTop: 12,
        marginHorizontal: 20,
        fontSize: 15,
        padding: 0,
        margin: 0,
        borderBottomWidth: 0
    },
    title: {
        fontSize: 26,
        fontWeight: '700',
        paddingTop: 5,
    },
    headerIcon: {
        width: 28,
        height: 28,
        alignItems: 'center',
        alignSelf: 'center',
        alignContent: 'center',
        resizeMode: 'stretch',
    },
});


export default styles
