import React from 'react';
import {Image, Text, TextInput, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import styles from "./styles";
import {Icon} from "@ant-design/react-native";
import {Color} from "../../config/variables";
import {useNavigation} from '@react-navigation/native';
import Badge from "@ant-design/react-native/es/badge";
import Constants from "../../middleware/constants";
import {FontAwesome5} from "@expo/vector-icons";

const Header = props => {
    let {
        title,
        left = "",
        search = true,
        cart = true,
        cartData = [],
    } = props;
    const navigation = useNavigation();

    return (
        <View style={styles.Header}>
            <View style={styles.left}>
                {left === "HOME" &&
                <TouchableOpacity style={styles.menuButton} onPress={() => navigation.navigate("Home")}>
                    <FontAwesome5 size={25} name={'home'} color={Color.grey1}/>
                </TouchableOpacity>}
                {left === "MENU" && <TouchableOpacity style={styles.menuButton} onPress={navigation.openDrawer}>
                    <FontAwesome5 size={25} name={'bars'} color={Color.grey1}/>
                </TouchableOpacity>}
                {left === "BACK" && <TouchableOpacity style={styles.menuButton} onPress={navigation.goBack}>
                    <FontAwesome5 size={25} name={'arrow-left'} color={Color.grey1}/>
                </TouchableOpacity>}
            </View>
            <View style={styles.center}>
                {!search && title && <Text style={styles.title}>{title}</Text>}
                {search && <TextInput
                    style={styles.searchBar}
                    value={props.filter.query}
                    onSubmitEditing={() => {
                        if (props.filter.query.length >= 2) navigation.navigate("product_see_all");
                    }}
                    placeholder={'Search Product..'}
                    onChangeText={text => props.changeQuery(text)}
                />}
            </View>
            <View style={styles.right}>
                {cart && <Badge
                    size={'small'}
                    text={cartData.reduce((a, b) => a + b.quantity, 0)} overflowCount={99}>
                    <TouchableOpacity onPress={() => navigation.navigate("cart_page")}
                                      style={styles.menuButton}>
                        <Image source={require("../../assets/images/shopping-cart.png")} style={styles.headerIcon}/>
                    </TouchableOpacity>
                </Badge>
                }
            </View>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        cartData: state.cart.detailedProduct.data || [],
        filter: state.seeAll.filters || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        changeQuery: query => dispatch({type: Constants.SEE_ALL_CHANGE_QUERY, payload: query}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
