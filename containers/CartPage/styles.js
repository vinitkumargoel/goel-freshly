import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color} from "../../config/variables";

const styles = StyleSheet.create({
    ...CommonStyles,
    CartPage: {
        // marginBottom: 150,
    },
    checkoutButton: {
        marginTop: 30,
        marginBottom: 200,
    },
    CartNoProducts: {
        padding: 20,
        backgroundColor: Color.white,
        height: '100%',
    },
    CartNoProductsIcon: {
        marginTop: 100,
        width: 200,
        height: 200,
        alignItems: 'center',
        alignSelf: 'center',
        alignContent: 'center',
        resizeMode: 'stretch',
    },
    IconContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
    },
    CartNoProductsText: {
        fontSize: 20,
        lineHeight: 28,
        fontWeight: '500',
        alignItems: 'center',
        textAlign: 'center',
    },
    dismissButton: {
        marginTop: 50,
    }
});


export default styles
