import React, {useEffect} from 'react';
import styles from "./styles";
import {ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import Constants from "../../middleware/constants";
import CartTile from "../CartTile";
import CartCalculations from "../../components/CartCalculations";
import CustomButton from "../../components/CustomButton";
import {useNavigation} from "@react-navigation/native";
import calculateCartValue from "../../utils/calculateCartValue";
import CartNoProducts from "./CartNoProducts";

const CartPage = props => {
    useEffect(() => {
        return navigation.addListener('focus', () => {
            props.refreshProducts(props.token);
        });
    }, [navigation]);
    useEffect(() => {
        props.updatedProduct();
    }, [props.data]);
    let navigation = useNavigation();
    let {totalPayable} = calculateCartValue(props.data);

    return (
        <View style={styles.CartPage}>
            <Header left={"BACK"} title={"Cart"} search={false}/>
            {props.data.length <= 0 && <CartNoProducts/>}
            {props.data.length > 0 && <ScrollView style={styles.appContainer}>
                {props.data.map((a, i) => <CartTile key={i} {...a.product} quantity={a.quantity}/>)}
                <CartCalculations data={props.data}/>
                <CustomButton icon={'shopping-basket'} style={styles.checkoutButton} type={'green'}
                              onPress={() => {
                                  props.setAmount(totalPayable);
                                  navigation.navigate("checkout");
                              }}>
                    Checkout
                </CustomButton>
            </ScrollView>}
        </View>
    );
};

function mapStateToProps(state) {
    return {
        token: state.profile.token || "",
        data: state.cart.detailedProduct.data || [],
        loading: state.cart.detailedProduct.loading || false,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setAmount: amount => dispatch({type: Constants.CHECKOUT_SET_AMOUNT, payload: amount}),
        updatedProduct: () => dispatch({type: Constants.GET_ALL_CART_PRODUCTS}),
        refreshProducts: token => dispatch({type: Constants.CART_REFRESH_PRODUCT, payload: {token}}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartPage);
