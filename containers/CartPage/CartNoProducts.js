import React from 'react';
import PropTypes from 'prop-types';
import {Image, Text, View} from "react-native";
import styles from "./styles";
import CustomButton from "../../components/CustomButton";
import {useNavigation} from '@react-navigation/native';

const CartNoProducts = props => {
    let navigation = useNavigation();

    return (
        <View style={styles.CartNoProducts}>
            <View style={styles.IconContainer}>
                <Image style={styles.CartNoProductsIcon} source={require("../../assets/images/no-cart.png")}/>
            </View>
            <Text style={styles.CartNoProductsText}>No Products in Cart!!</Text>
            <CustomButton style={styles.dismissButton} type={'green'} onPress={() => navigation.navigate("Home")}>
                Continue Shopping
            </CustomButton>
        </View>
    );
};

CartNoProducts.propTypes = {};

export default CartNoProducts;
