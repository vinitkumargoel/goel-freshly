import React, {useEffect, useState} from 'react';
import {Text, View} from "react-native";
import styles from "./styles";
import {connect} from "react-redux";
import CustomButton from "../../components/CustomButton";
import CustomTextBox from "../../components/CustomTextBox";
import isEmpty from "lodash/isEmpty";
import AlertBox from "../../components/AlertBox";
import utils from "../../config/utils";
import Constants from "../../middleware/constants";

const RegisterScreen = ({navigation, ...props}) => {
    let [state, setState] = useState({
        name: "",
        email: "",
        mobile: "",
        password: "",
        confirmPassword: "",
    });
    useEffect(() => {
        console.log(props.register);
        if (!isEmpty(props.registerAPI.error)) {
            AlertBox("Error while Register!!")
        }
        if (!isEmpty(props.registerAPI.data)) {
            props.setToken(props.registerAPI.data.token);
            props.setProfile(props.registerAPI.data.user);
            navigation.navigate("Home");
        }
    }, [props.registerAPI]);
    const onRegister = () => {
        if (isEmpty(state.email) ||
            isEmpty(state.email) ||
            isEmpty(state.mobile) ||
            isEmpty(state.password) ||
            isEmpty(state.confirmPassword)) {
            AlertBox('Please Enter All Details!')
        } else if (state.password !== state.confirmPassword) {
            AlertBox('Passwords Doesn\'t Match!')
        } else if (!utils.isEmail(state.email)) {
            AlertBox('Please Enter Correct Email ID!')
        } else {
            props.registerUser(state);
            // navigation.navigate("verification")
        }
    };
    return (
        <View style={styles.RegisterScreen}>
            <Text style={styles.title}>Register</Text>
            <CustomTextBox
                autoCapitalize={'words'}
                styles={styles.inputBox}
                placeholder={'Full Name'}
                value={state.name}
                onChangeText={text => setState({...state, name: text})}
            />
            <CustomTextBox
                autoCapitalize={'none'}
                keyboardType={"email-address"}
                styles={styles.inputBox}
                placeholder={'Email'}
                value={state.email}
                onChangeText={text => setState({...state, email: text})}
            />
            <CustomTextBox
                autoCapitalize={'none'}
                styles={styles.inputBox}
                maxLength={10}
                keyboardType={"number-pad"}
                placeholder={'Mobile'}
                value={state.mobile}
                onChangeText={text => setState({...state, mobile: text})}
            />
            <CustomTextBox
                autoCapitalize={'none'}
                secureTextEntry={true}
                styles={styles.inputBox}
                placeholder={'Password'}
                value={state.password}
                onChangeText={text => setState({...state, password: text})}
            />

            <CustomTextBox
                autoCapitalize={'none'}
                secureTextEntry={true}
                styles={styles.inputBox}
                placeholder={'Confirm Password'}
                value={state.confirmPassword}
                onChangeText={text => setState({...state, confirmPassword: text})}
            />

            <CustomButton type={'clear'} onPress={onRegister}>
                Register
            </CustomButton>
            <CustomButton type={'clean'} onPress={() => navigation.navigate("login")}>
                Already Login?
            </CustomButton>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        registerAPI: state.auth.register || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        registerUser: payload => dispatch({type: Constants.AUTH_REGISTER_USER, payload}),
        setToken: token => dispatch({type: Constants.PROFILE_SET_TOKEN, payload: token}),
        setProfile: payload => dispatch({type: Constants.PROFILE_SET_DETAILS, payload}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);
