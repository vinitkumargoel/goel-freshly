import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";

const styles = StyleSheet.create({
    ...CommonStyles,
    RegisterScreen: {
        padding: 20,
        marginTop: 80,
    },
    inputBox: {
        marginTop: 5,
        marginHorizontal: 20,
    },
    title: {
        fontSize: 30,
        fontWeight: '700',
        paddingTop: 5,
        textAlign: 'center',
        marginBottom: 40,
    },

});


export default styles
