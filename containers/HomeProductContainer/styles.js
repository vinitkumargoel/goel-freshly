import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";

const styles = StyleSheet.create({
    ...CommonStyles,
    HomeProductContainer: {
        marginBottom: 150,
    },
    ProductContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    label: {
        fontSize: 20,
        fontWeight: "bold",
        paddingBottom: 10,
    }
});


export default styles
