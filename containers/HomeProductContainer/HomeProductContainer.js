import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {Text, View} from "react-native";
import styles from "./styles";
import ProductThumb from "../../components/ProductThumb";
import {connect} from "react-redux";
import Constants from "../../middleware/constants";

const HomeProductContainer = props => {
    useEffect(() => {
        props.getPopularProducts(4, props.token)
    }, []);
    return (
        <View style={styles.HomeProductContainer}>
            <Text style={styles.label}>{props.label}</Text>
            <View style={styles.ProductContainer}>{props.data.map((a, i) => <ProductThumb {...a} key={i}/>)}</View>
        </View>
    );
};

HomeProductContainer.propTypes = {
    label: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
    return {
        token: state.profile.token || "",
        data: state.product_home.popular_products.data || [],
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getPopularProducts: (limit, token) => dispatch({
            type: Constants.GET_POPULAR_PRODUCT_DETAILS,
            payload: {limit, token}
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeProductContainer);
