import React from "react";

import ProfilePage from "./ProfilePage";
import createStackNavigator from "@react-navigation/stack/src/navigators/createStackNavigator";
import MangeAddress from "../MangeAddress";
import ChangePassword from "../ChangePassword";
import ChangeMobileNumber from "../ChangeMobileNumber";
import ChangeEmailId from "../ChangeEmailId";
import AddAddressPage from "../AddAddressPage";
import VerificationCodeSecureScreen from "../VerificationCodeSecureScreen";
import SelectAddress from "../SelectAddress";

const Stack = createStackNavigator();
const Component = ({navigation, route}) => {
    navigation.setOptions({
        headerMode: 'none',
    });
    return (
        <Stack.Navigator
            headerMode='none'
            screenOptions={{}}>
            <Stack.Screen
                name="profile"
                initialParams={{navigation, route}}
                component={ProfilePage}
                options={{title: 'Profile', headerMode: 'none',}}
            />
            <Stack.Screen
                name="manage_address"
                initialParams={{navigation, route}}
                component={MangeAddress}
                options={{title: 'manage_address', headerMode: 'none',}}
            />
            <Stack.Screen
                name="change_password"
                initialParams={{navigation, route}}
                component={ChangePassword}
                options={{title: 'change_password', headerMode: 'none',}}
            />
            <Stack.Screen
                name="change_mobile"
                initialParams={{navigation, route}}
                component={ChangeMobileNumber}
                options={{title: 'change_mobile', headerMode: 'none',}}
            />
            <Stack.Screen
                name="change_email"
                initialParams={{navigation, route}}
                component={ChangeEmailId}
                options={{title: 'change_email', headerMode: 'none',}}
            />
            <Stack.Screen
                name="add_address"
                initialParams={{navigation, route}}
                component={AddAddressPage}
                options={{title: 'add_address', headerMode: 'none',}}
            />
            <Stack.Screen
                name="verification_secure"
                initialParams={{navigation, route}}
                component={VerificationCodeSecureScreen}
                options={{title: 'add_address', headerMode: 'none',}}
            />

        </Stack.Navigator>
    )
};
export default Component;

