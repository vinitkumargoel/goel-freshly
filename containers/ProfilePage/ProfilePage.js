import React from 'react';
import styles from "./styles";
import {Image, ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import moment from "moment";
import {List} from "@ant-design/react-native";
import Item from "@ant-design/react-native/es/list/ListItem";
import {FontAwesome5} from "@expo/vector-icons";

const ListIcon = ({name}) => <FontAwesome5 color={'#444444'} size={20} name={name}/>;

const ProfilePage = ({navigation, route, details}) => {
    console.log(details);
    return (
        <View style={styles.ProfilePage}>
            <Header left={"BACK"} title={"Profile"} search={false}/>
            <ScrollView style={styles.appContainer}>
                <View style={styles.DataContainer}>
                    <View style={styles.InfoContainer}>
                        <Text style={styles.name}>{details.name}</Text>
                        <Text style={styles.email}>{details.email}</Text>
                        <Text style={styles.mobile}>+91 {details.mobile}</Text>
                    </View>
                    <View style={styles.imageContainer}>
                        <Image style={styles.image} source={{uri: `${details.image}`}}/>
                    </View>
                </View>
                <View style={styles.ListContainer}>
                    <List>
                        <Item style={styles.Item}
                              arrow="horizontal"
                              extra={<ListIcon name={'paper-plane'}/>}
                              onPress={() => navigation.navigate("orders")}>
                            Orders
                        </Item>
                        <Item style={styles.Item}
                              arrow="horizontal"
                              extra={<ListIcon name={'address-book'}/>}
                              onPress={() => navigation.navigate("manage_address")}>
                            Manage Address
                        </Item>
                        <Item style={styles.Item}
                              arrow="horizontal"
                              extra={<ListIcon name={'key'}/>}
                              onPress={() => navigation.navigate("change_password")}>
                            Change Password
                        </Item>
                        <Item style={styles.Item}
                              arrow="horizontal"
                              extra={<ListIcon name={'at'}/>}
                              onPress={() => navigation.navigate("change_email")}>
                            Change Email Id
                        </Item>
                        <Item style={styles.Item}
                              arrow="horizontal"
                              extra={<ListIcon name={'mobile-alt'}/>}
                              onPress={() => navigation.navigate("change_mobile")}>
                            Change Mobile Number
                        </Item>
                    </List>
                </View>
            </ScrollView>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        details: state.profile.details || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
