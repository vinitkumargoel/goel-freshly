import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import Colors from "../../constants/Colors";

const styles = StyleSheet.create({
    ...CommonStyles,
    ProfilePage: {},
    DataContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: "#ececec",
        paddingBottom: 20,
    },
    InfoContainer: {
        width: '60%',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageContainer: {
        width: '40%',
    },
    name: {
        fontSize: 26,
        fontWeight: '700',
    },
    addedDate: {
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 25,
    },
    image: {
        width: 120,
        height: 120,
        borderRadius: 60,
        resizeMode: 'stretch',
    },
    mobile: {
        fontSize: 15,
        fontWeight: '400',
        lineHeight: 25,
    },
    email: {
        lineHeight: 25,
        fontSize: 15,
        fontWeight: '400',
    },
    ListContainer: {},
    Item: {
        paddingVertical: 7,
    }
});


export default styles
