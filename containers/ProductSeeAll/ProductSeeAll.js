import React, {useEffect, useState} from 'react';
import {ScrollView, View} from "react-native";
import styles from "./styles";
import Header from "../Header/Header";
import {connect} from "react-redux";
import ProductThumb from "../../components/ProductThumb";
import ResultsToolbar from "./ResultsToolbar";
import FilterModal from "./FilterModal";
import Constants from "../../middleware/constants";

const ProductSeeAll = props => {
    let {data = [], filter, count, token} = props;
    let [modal, setModal] = useState(false);
    useEffect(() => {
        props.getProduct(filter, token);
    }, [filter]);
    return (
        <View style={styles.ProductSeeAll}>
            <Header left={"HOME"} search={true}/>
            <ResultsToolbar count={count} onFilterClick={() => setModal(!modal)}/>
            <ScrollView
                // onScrollEndDrag={() => props.changePage(props.filter.page_number + 1)}
                style={styles.appContainer}>
                <View style={styles.ResultsContainer}>
                    {data.map((a, i) => <ProductThumb {...a} key={i}/>)}
                </View>
            </ScrollView>
            <FilterModal onFilterClick={() => setModal(!modal)} visible={modal}/>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        token: state.profile.token || "",
        filter: state.seeAll.filters || {},
        data: state.product.seeAll.data || [],
        count: state.product.seeAll.count || 0,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        changePage: page_number => dispatch({type: Constants.SEE_ALL_CHANGE_PAGE_NUMBER, payload: page_number}),
        getProduct: (body, token) => dispatch({type: Constants.GET_PRODUCT_SEE_ALL, payload: {body, token}}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductSeeAll);
