import React, {useState} from 'react';
import {Modal, ScrollView, Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import SortFilter from "./SortFilter";
import CategoryFilter from "./CategoryFilter";
import Constants from "../../middleware/constants";
import {connect} from "react-redux";

let Tabs = [{
    label: "Sort",
    component: <SortFilter/>,
}, {
    label: "Category",
    component: <CategoryFilter/>
}];

const FilterModal = props => {
    let [index, setIndex] = useState(0);
    return (
        <Modal
            transparent={true}
            visible={props.visible}>
            <View style={styles.ModalContainer}>
                <View style={styles.ModelBody}>
                    <View style={styles.ModelHeader}>
                        <Text style={styles.ModalTitle}>Filter & Sort</Text>
                    </View>
                    <View style={styles.FilterContainer}>
                        <ScrollView style={styles.VerticalTabsContainer}>
                            {Tabs.map((a, i) => <TouchableOpacity
                                onPress={() => setIndex(i)}
                                key={i} style={styles.TabContainer(i === index)}>
                                <Text style={styles.TabText(i === index)}>{a.label}</Text>
                            </TouchableOpacity>)}
                        </ScrollView>
                        <ScrollView style={styles.VerticalTabsViewContainer}>
                            {Tabs[index].component}
                        </ScrollView>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity style={styles.ClearButton} onPress={props.clearFilter}>
                            <Text style={styles.CLearButtonText}>Clear Filters</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.ModalCloseButton} onPress={props.onFilterClick}>
                            <Text style={styles.ModalCloseButtonText}>Apply Filters</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    );
};

function mapStateToProps(state) {
    return {
        filter: state.seeAll.filters || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        clearFilter: () => dispatch({type: Constants.SEE_ALL_CLEAR_FILTERS}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterModal);
