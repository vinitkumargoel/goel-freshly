import React from 'react';
import {View} from "react-native";
import styles from "./styles";
import RadioForm from 'react-native-simple-radio-button';
import {connect} from "react-redux";
import Constants from "../../middleware/constants";

const radio_props = [
    {label: 'Popularity', value: 'POPULARITY'},
    {label: 'High To Low', value: 'HIGH-TO-LOW'},
    {label: 'Low To High', value: 'LOW-TO-HIGH'},
    {label: 'NEWEST', value: 'NEWEST'}
];
const SortFilter = props => {
    return (
        <View style={styles.FilterViewContainer}>
            <RadioForm
                radio_props={radio_props}
                initial={radio_props.findIndex(a => a.value === props.filter.sort)}
                onPress={(value) => {
                    props.changeSort(value)
                }}
            />
        </View>
    );
};


function mapStateToProps(state) {
    return {
        filter: state.seeAll.filters || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        changeSort: value => dispatch({type: Constants.SEE_ALL_CHANGE_SORT, payload: value}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SortFilter);
