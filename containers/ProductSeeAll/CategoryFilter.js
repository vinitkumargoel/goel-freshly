import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import Constants from "../../middleware/constants";
import {connect} from "react-redux";
import isEmpty from "lodash/isEmpty";
import {FontAwesome5} from "@expo/vector-icons";

const CategoryFilter = props => {
    let {l0, l1} = props;
    const setL0 = payload => props.changeCategoryL0(payload);
    const setL1 = payload => props.changeCategoryL1(payload);
    const getL0Categories = data => {
        let final = [];
        for (let i of data) {
            if (i.level === 0) {
                final.push(<TouchableOpacity
                        onPress={() => setL0(i)}
                        style={styles.L0CatContainer(l0._id === i._id)}>
                        <Text tyle={styles.L0CatText}>{i.title}</Text>
                    </TouchableOpacity>
                )
            }
        }
        return final;
    };
    const getL1Categories = (data, l0) => {
        let final = [];
        for (let i of data) {
            if (i.parentId === l0._id) {
                final.push(<TouchableOpacity
                        onPress={() => setL1(i)}
                        style={styles.L1CatContainer(l1._id === i._id)}>
                        <Text tyle={styles.L1CatText}>{i.title}</Text>
                    </TouchableOpacity>
                )
            }
        }
        return final;
    };
    return (
        <View style={styles.FilterViewContainer}>
            {!isEmpty(l0) && <TouchableOpacity
                onPress={() => {
                    setL0({});
                    setL1({})
                }}
                style={styles.L0CatActiveContainer}>
                <Text style={styles.L0CatActiveText}><FontAwesome5 name={"long-arrow-alt-left"}/> {l0.title}</Text>
            </TouchableOpacity>}
            {isEmpty(l0) && getL0Categories(props.categories)}
            {!isEmpty(l0) && getL1Categories(props.categories, l0)}
        </View>
    );
};

function mapStateToProps(state) {
    return {
        filter: state.seeAll.filters || {},
        categories: state.category.all.data || [],
        l0: state.seeAll.filters.category_l1 || {},
        l1: state.seeAll.filters.category_l2 || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        changeCategoryL0: payload => dispatch({type: Constants.SEE_ALL_CHANGE_CATEGORY_L1, payload}),
        changeCategoryL1: payload => dispatch({type: Constants.SEE_ALL_CHANGE_CATEGORY_L2, payload}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryFilter);
