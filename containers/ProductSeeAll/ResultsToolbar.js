import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import styles from "./styles";

const ResultsToolbar = props => {
    return (
        <View style={styles.toolbar}>
            <Text style={styles.count}>Over {props.count} Products</Text>
            <TouchableOpacity onPress={props.onFilterClick} style={styles.filterButton}>
                <Text style={styles.filterButtonText}>Filter</Text>
            </TouchableOpacity>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        ...state
    };
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultsToolbar);
