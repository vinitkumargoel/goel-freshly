import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color, MAIN_PADDING} from "../../config/variables";

const styles = StyleSheet.create({
    ...CommonStyles,
    ProductSeeAll: {},
    ResultsContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 200,
    },
    toolbar: {
        backgroundColor: Color.white,
        paddingBottom: 20,
        paddingHorizontal: MAIN_PADDING,
        flexDirection: 'row',
        borderColor: '#eff0f1',
        borderBottomWidth: 1,
        color: '#eff0f1'
    },
    count: {
        paddingTop: 10,
        width: '80%',
        color: '#3d3d3d'
    },
    filterButton: {
        flexDirection: 'row',
        // width: '20%',
        alignItems: 'center',
        textAlign: 'center',
        alignContent: 'center',
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'black',
    },
    filterButtonText: {
        alignItems: 'center',
        textAlign: 'center',
        alignContent: 'center',
        color: '#3d3d3d',
        fontSize: 15,
    },
    ModalContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.41,
        shadowRadius: 9.11,
        elevation: 14,
    },
    ModelBody: {
        borderRadius: 12,
        backgroundColor: 'white',
        width: '100%',
        height: 600,
    },
    ModelHeader: {
        paddingBottom: 10,
        padding: 20,
        borderTopWidth: 1,
        borderTopColor: '#ececec',
        borderBottomWidth: 1,
        borderBottomColor: '#ececec',
    },
    ModalTitle: {
        textAlign: 'center',
        fontSize: 18,
        fontWeight: '700',
    },
    ClearButton: {
        margin: 20,
        borderRadius: 14,
        alignContent: 'flex-end',
    },
    CLearButtonText: {
        paddingVertical: 20,
        paddingHorizontal: 10,
        color: Color.black,
        fontSize: 16,
        fontWeight: '600',
    },
    ModalCloseButton: {
        margin: 20,
        borderRadius: 14,
        alignContent: 'flex-end',
        backgroundColor: Color.green
    },
    ModalCloseButtonText: {
        marginHorizontal: 30,
        paddingVertical: 20,
        paddingHorizontal: 30,
        color: 'white',
        fontSize: 18,
        fontWeight: '600',
    },
    FilterContainer: {
        flexDirection: 'row',
    },
    VerticalTabsContainer: {
        width: '30%',
        height: 420,
        backgroundColor: "#eff0f1",

    },
    VerticalTabsViewContainer: {
        width: '70%',
        height: 420,
    },
    TabContainer: (focus = false) => {
        return {
            paddingHorizontal: 20,
            paddingVertical: 25,
            backgroundColor: focus ? Color.white : "#eff0f1",
        }
    },
    TabText: (focus = false) => {
        return {
            color: focus ? Color.green : Color.black,
            fontWeight: '600',
            fontSize: 15,
        }
    },
    FilterViewContainer: {
        padding: 20,
    },
    L0CatContainer: (focus = false) => {
        return {
            paddingBottom: 20,
            backgroundColor: focus ? "#eff0f1" : Color.white,
        }
    },
    L0CatText: {
        fontSize: 15,
        fontWeight: '600',
    },
    L0CatActiveContainer: {
        marginBottom: 10,
    },
    L0CatActiveText: {
        color: Color.green,
    },
    L1CatContainer: (focus = false) => {
        return {
            paddingVertical: 10,
            paddingHorizontal: 10,
            borderRadius: 5,
            backgroundColor: focus ? "#eff0f1" : Color.white,
        }
    },
    L1CatText: {},
});


export default styles;
