import React from 'react';
import {ScrollView, Text, TouchableOpacity} from "react-native";
import styles from "./styles";
import {connect} from "react-redux";
import {FontAwesome5} from "@expo/vector-icons";
import CategoriesPanel from "./CategoriesPanel";
import {useNavigation} from "@react-navigation/native";
import Constants from "../../middleware/constants";

const DrawerScreen = props => {
    let navigation = useNavigation();
    return (
        <ScrollView style={styles.DrawerScreen}>
            <TouchableOpacity style={styles.LinkContainer} onPress={() => navigation.navigate("Home")}>
                <FontAwesome5 size={17} iconStyle={styles.LinkIcon} name={'home'}/>
                <Text style={styles.LinkText}>Home</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.LinkContainer} onPress={() => navigation.navigate("cart_page")}>
                <FontAwesome5 size={17} iconStyle={styles.LinkIcon} name={'shopping-cart'}/>
                <Text style={styles.LinkText}>Cart</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.LinkContainer} onPress={() => navigation.navigate("orders")}>
                <FontAwesome5 size={17} iconStyle={styles.LinkIcon} name={'paper-plane'}/>
                <Text style={styles.LinkText}>Orders</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.LinkContainer} onPress={() => navigation.navigate("profile")}>
                <FontAwesome5 size={17} iconStyle={styles.LinkIcon} name={'user-circle'}/>
                <Text style={styles.LinkText}>Profile</Text>
            </TouchableOpacity>
            {/*<TouchableOpacity style={styles.LinkContainer} onPress={() => navigation.navigate("settings")}>*/}
            {/*    <FontAwesome5 size={17} iconStyle={styles.LinkIcon} name={'sliders-h'}/>*/}
            {/*    <Text style={styles.LinkText}>Settings</Text>*/}
            {/*</TouchableOpacity>*/}
            <TouchableOpacity style={styles.LinkContainer} onPress={() => {
                props.logoutUser();
                props.resetCart();
                navigation.navigate("login");
            }}>
                <FontAwesome5 size={17} iconStyle={styles.LinkIcon} name={'sign-out-alt'}/>
                <Text style={styles.LinkText}>Logout</Text>
            </TouchableOpacity>
            <CategoriesPanel/>
        </ScrollView>
    );
};

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return {
        resetCart: () => dispatch({type: Constants.CART_RESET}),
        logoutUser: () => dispatch({type: Constants.AUTH_LOGOUT})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerScreen);
