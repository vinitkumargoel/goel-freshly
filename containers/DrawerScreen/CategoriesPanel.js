import React, {useState} from 'react';
import {connect} from "react-redux";
import {Accordion, List} from "@ant-design/react-native";
import {Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import Constants from "../../middleware/constants";
import {useNavigation} from '@react-navigation/native';

const CategoriesPanel = props => {
    let [category, setCategory] = useState([]);
    const onChange = activeSections => setCategory(activeSections);
    let navigation = useNavigation();
    const onNavigate = (l1, l2) => {
        props.changeCategory({l1, l2});
        navigation.navigate("product_see_all");
    };
    const getCategories = data => {
        let final = [];
        for (let i of data) {
            if (i.level === 0) {
                let sub = [];
                for (let j of data) {
                    if (j.parentId === i._id) {
                        sub.push(<List.Item><TouchableOpacity
                            style={{marginLeft: 10}}
                            onPress={() => onNavigate(i, j)}><Text>{j.title}</Text></TouchableOpacity></List.Item>)
                    }
                }
                final.push(<Accordion.Panel header={i.title}><List>{sub}</List></Accordion.Panel>);
            }
        }
        return final;
    };
    return (
        <View style={styles.CategoryContainer}>
            <Text style={styles.mainText}>Categories</Text>
            <Accordion
                onChange={onChange}
                activeSections={category}
            >
                {getCategories(props.data)}
            </Accordion>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        data: state.category.all.data || [],
    };
}

function mapDispatchToProps(dispatch) {
    return {
        changeCategory: payload => dispatch({type: Constants.SEE_ALL_CHANGE_CATEGORY, payload}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesPanel);

