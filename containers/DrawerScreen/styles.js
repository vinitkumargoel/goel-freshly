import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color, TOP_PADDING} from "../../config/variables";
import Colors from "../../constants/Colors";

const styles = StyleSheet.create({
    ...CommonStyles,
    DrawerScreen: {
        paddingTop: TOP_PADDING,
        marginHorizontal: 0,
        paddingHorizontal: 0,
        width: '100%',
        marginTop: 10,
    },
    DrawerItemLabel: {},
    DrawerItem: {
        margin: 0,
        padding: 0,
        alignSelf: 'stretch',
        width: '100%'
    },
    mainText: {
        fontSize: 20,
        fontWeight: "bold",
        marginLeft: 20,
        marginVertical: 10,
        color: Colors.drawerMainText,
    },
    CategoryContainer: {
        marginBottom: 100,
    },
    LinkContainer: {
        flexDirection: 'row',
        marginLeft: 20,
        paddingVertical: 10,
        borderBottomColor: "#ececec",
        // borderBottomWidth: 1,
    },
    LinkIcon: {
        marginTop: 10,
    },
    LinkText: {
        marginLeft: 10,
        fontSize: 18,
    },
    LinkBadge: {
        marginLeft: 10,
        padding: 3,
        fontWeight: "700",
        color: Color.green,
        borderRadius: 13,
        fontSize: 15,
    }
});


export default styles
