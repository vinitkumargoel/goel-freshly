import React, {useEffect} from 'react';
import styles from "./styles";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import Constants from "../../middleware/constants";
import AddressCard from "../../components/AddressCard";
import CustomButton from "../../components/CustomButton";

const SelectAddress = ({navigation, route, ...props}) => {
    useEffect(() => {
        return navigation.addListener('focus', () => {
            props.getAllAddress(props.token);
        });
    }, [navigation]);
    return (
        <View style={styles.SelectAddress}>
            <Header left={"BACK"} title={"Select Address"} cart={false} search={false}/>
            <ScrollView style={styles.appContainer}>
                <CustomButton icon={'plus'}
                              style={styles.checkoutButton}
                              type={'green'}
                              onPress={() => navigation.navigate("add_address_2")}>
                    Add New Address
                </CustomButton>
                {props.data.map((a, i) => <AddressCard
                    {...a}
                    onPress={() => {
                        // console.log(a)
                        props.setAddress(a);
                        navigation.goBack()
                    }}
                    removeButton={false}
                    key={i}/>)}
                <View style={{marginBottom: 200}}/>
            </ScrollView>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        data: state.address.all.data || [],
        token: state.profile.token || "",
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setAddress: address => dispatch({type: Constants.CHECKOUT_SET_ADDRESS, payload: address}),
        getAllAddress: token => dispatch({type: Constants.ADDRESS_GET_ALL, payload: {token}}),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectAddress);
