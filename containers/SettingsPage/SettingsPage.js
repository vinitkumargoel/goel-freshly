import React from 'react';
import styles from "./styles";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";

const SettingsPage = ({navigation, route}) => {
    return (
        <View style={styles.SettingsPage}>
            <Header left={"BACK"} title={"Settings"} search={false}/>
            <ScrollView style={styles.appContainer}>
            </ScrollView>
        </View>
    );
};

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
