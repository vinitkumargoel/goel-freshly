import React from 'react';
import {Image, Text, View} from "react-native";
import styles from "./styles";
import CustomButton from "../../components/CustomButton";
import {useNavigation} from "@react-navigation/native";

const NoOrders = props => {
    let navigation = useNavigation();

    return (
        <View style={styles.NoOrders}>
            <View style={styles.IconContainer}>
                <Image style={styles.NoOrdersIcon} source={require("../../assets/images/no-order.png")}/>
            </View>
            <Text style={styles.NoOrdersText}>No Orders Yet!!</Text>
            <CustomButton style={styles.dismissButton} type={'green'} onPress={() => navigation.navigate("Home")}>
                Continue Shopping
            </CustomButton>
        </View>
    );
};

export default NoOrders;
