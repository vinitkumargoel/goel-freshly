import React, {useEffect} from 'react';
import styles from "./styles";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import Constants from "../../middleware/constants";
import NoOrders from "./NoOrders";
import OrderTile from "../../components/OrderTile/OrderTile";

const OrderPage = ({navigation, route, ...props}) => {
    useEffect(() => {
        return navigation.addListener('focus', () => {
            props.getAllOrders(props.token);
        });
    }, [navigation]);
    return (
        <View style={styles.OrderPage}>
            <Header left={"BACK"} title={"Orders"} search={false}/>
            <ScrollView style={styles.appContainer}>
                {!props.allOrders.loading && props.allOrders.data.length === 0 && <NoOrders/>}
                {!props.allOrders.loading && props.allOrders.data.length !== 0 && props.allOrders.data.map((a, i) =>
                    <OrderTile onPress={() => navigation.navigate("order_details", {id: a._id})} {...a} key={i}/>)}
                <View style={{marginBottom: 200}}/>
            </ScrollView>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        allOrders: state.order.all || [],
        token: state.profile.token || "",
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getAllOrders: token => dispatch({type: Constants.ORDER_GET_ALL, payload: {token}}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderPage);
