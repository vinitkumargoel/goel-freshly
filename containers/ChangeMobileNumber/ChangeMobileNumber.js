import React, {useEffect, useState} from 'react';
import styles from "./styles";
import {ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import CustomTextBox from "../../components/CustomTextBox";
import CustomButton from "../../components/CustomButton";
import data from "./data";

const ChangeMobileNumber = ({navigation, route, ...props}) => {
    let [state, setState] = useState({
        existing: props.profile.mobile,
        new: "",
    });
    let [error, setError] = useState("");
    useEffect(() => {
        if (state.new.trim() === state.existing.trim()) {
            setError(data.same_mobile)
        } else {
            setError("");
        }
    }, [state.new]);
    return (
        <View>
            <Header left={"BACK"} cart={false} search={false}/>
            <View style={{...styles.appContainer, ...styles.ChangeMobileNumber}}>
                <Text style={styles.largeTitle}>Change Mobile Number</Text>
                <CustomTextBox
                    editable={true}
                    label={'Old Mobile Number'}
                    keyboardType={"numeric"}
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    placeholder={'Existing Mobile'}
                    value={state.existing}
                    onChangeText={() => console.log("")}
                />
                <CustomTextBox
                    editable={false}
                    keyboardType={"numeric"}
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    error={error !== ""}
                    errorMessage={error}
                    placeholder={'Enter New Mobile Number'}
                    value={state.new}
                    onChangeText={text => setState({...state, new: text})}
                />
                <CustomButton type={'clear'} onPress={() => !error && navigation.navigate("verification_secure")}>
                    Change
                </CustomButton>
            </View>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        profile: state.profile.details || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeMobileNumber);
