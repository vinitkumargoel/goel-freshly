import React from "react";

import LoginScreen from "./LoginScreen";
import createStackNavigator from "@react-navigation/stack/src/navigators/createStackNavigator";
import RegisterScreen from "../RegisterScreen";
import VerificationCodeScreen from "../VerificationCodeScreen";

const Stack = createStackNavigator();

const Component = ({navigation, route}) => {
    navigation.setOptions({
        headerMode: 'none',
    });
    return (
        <Stack.Navigator
            headerMode='none'
            screenOptions={{}}>
            <Stack.Screen
                name="login"
                initialParams={{navigation, route}}
                component={LoginScreen}
                options={{title: 'Home', headerMode: 'none',}}
            />
            <Stack.Screen
                name="register"
                initialParams={{navigation, route}}
                component={RegisterScreen}
                options={{title: 'Product Details', headerMode: 'none',}}
            />

            <Stack.Screen
                name="verification"
                initialParams={{navigation, route}}
                component={VerificationCodeScreen}
                options={{title: 'Product Details', headerMode: 'none',}}
            />
        </Stack.Navigator>
    )
};
export default Component;
