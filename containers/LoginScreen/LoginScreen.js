import React, {useEffect, useState} from 'react';
import {Text, View} from "react-native";
import Constants from "../../middleware/constants";
import {connect} from "react-redux";
import CustomButton from "../../components/CustomButton";
import styles from "./styles";
import CustomTextBox from "../../components/CustomTextBox";
import AlertBox from "../../components/AlertBox";

import isEmpty from "lodash/isEmpty";
import utils from "../../config/utils";

const LoginScreen = ({navigation, ...props}) => {
    let [state, setState] = useState({
        email: "vinit@gmail.com",
        password: "123456789",
    });

    useEffect(() => {
        if (!isEmpty(props.checkToken.data)) {
            navigation.navigate("Home");
        }
    }, [props.checkToken]);

    useEffect(() => {
        if (!isEmpty(props.loginAPI.error)) {
            AlertBox("Error while login!!")
        }
        if (!isEmpty(props.loginAPI.data)) {
            props.setToken(props.loginAPI.data.token);
            props.setProfile(props.loginAPI.data.user);
            navigation.navigate("Home");
        }
    }, [props.loginAPI]);

    const onLogin = () => {
        if (isEmpty(state.email) || isEmpty(state.password)) {
            AlertBox('Please Enter Details!')
        } else if (!utils.isEmail(state.email)) {
            AlertBox('Please Enter Correct Email ID!')
        } else {
            props.loginUser(state);
        }
    };

    return (
        <View style={styles.LoginScreen}>
            <Text style={styles.title}>Login</Text>
            <CustomTextBox
                keyboardType={"email-address"}
                autoCapitalize={'none'}
                styles={styles.inputBox}
                placeholder={'Email'}
                value={state.email}
                onChangeText={text => setState({...state, email: text})}
            />
            <CustomTextBox
                autoCapitalize={'none'}
                secureTextEntry={true}
                styles={styles.inputBox}
                placeholder={'Password'}
                value={state.password}
                onChangeText={text => setState({...state, password: text})}
            />
            <CustomButton type={'clear'} onPress={onLogin}>Login</CustomButton>
            <CustomButton type={'clean'} onPress={() => navigation.navigate("register")}>New User?</CustomButton>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        loginAPI: state.auth.login || {},
        token: state.profile.token || "",
        checkToken: state.auth.checkToken || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        verifyToken: token => dispatch({type: Constants.AUTH_CHECK_TOKEN, payload: {token}}),
        setToken: token => dispatch({type: Constants.PROFILE_SET_TOKEN, payload: token}),
        setProfile: payload => dispatch({type: Constants.PROFILE_SET_DETAILS, payload}),
        loginUser: payload => dispatch({type: Constants.AUTH_LOGIN_USER, payload}),
        changeCategory: payload => dispatch({type: Constants.SEE_ALL_CHANGE_CATEGORY, payload}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
