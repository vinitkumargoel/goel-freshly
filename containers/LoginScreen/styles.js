import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";

const styles = StyleSheet.create({
    ...CommonStyles,
    LoginScreen: {
        padding: 20,
        marginTop: 100,
    },
    inputBox: {
        marginTop: 12,
        marginHorizontal: 20,
    },
    title: {
        fontSize: 30,
        fontWeight: '700',
        paddingTop: 5,
        textAlign: 'center',
        marginBottom: 60,
    }

});


export default styles
