import React, {useState} from 'react';
import {Text, View} from "react-native";
import styles from "./styles";
import {connect} from "react-redux";
import CustomTextBox from "../../components/CustomTextBox";
import CustomButton from "../../components/CustomButton";
import Header from "../Header/Header";

const VerificationCodeSecureScreen = ({navigation}) => {
    let [state, setState] = useState({
        v0: "",
        v1: "",
        v2: "",
        v3: "",
    });
    return (
        <View style={styles.VerificationCodeSecureScreen}>
            <Header left={"BACK"} cart={false} search={false}/>
            <View style={styles.appContainer}>
                <Text style={styles.title}>Verification Code</Text>
                <View style={styles.CodeContainer}>

                </View>
                <CustomButton style={styles.button}
                              type={'clear'}
                              onPress={() => navigation.navigate("profile")}>
                    Next
                </CustomButton>
            </View>
        </View>
    );
};

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(VerificationCodeSecureScreen);
