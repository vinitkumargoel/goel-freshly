import React, {useEffect} from 'react';
import styles from "./styles";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import CustomButton from "../../components/CustomButton";
import Constants from "../../middleware/constants";
import AddressCard from "../../components/AddressCard";
import isEmpty from "lodash/isEmpty";
import AlertBox from "../../components/AlertBox";

const MangeAddress = ({navigation, route, ...props}) => {
    useEffect(() => {
        return navigation.addListener('focus', () => {
            props.getAllAddress(props.token);
        });
    }, [navigation]);
    useEffect(() => {
        props.getAllAddress(props.token);
    }, []);
    useEffect(() => {
        if (!isEmpty(props.removeAPI.error)) {
            AlertBox("Error while Removing!!")
        }
        if (!isEmpty(props.removeAPI.data)) {
            props.getAllAddress(props.token);
        }
    }, [props.removeAPI]);
    const onRemove = (id) => {
        props.removeAddress(id, props.token);
    };
    return (
        <View style={styles.MangeAddress}>
            <Header left={"BACK"} title={"Mange Address"} cart={false} search={false}/>
            <ScrollView style={{...styles.appContainer}}>
                <CustomButton icon={'plus'} style={styles.checkoutButton} type={'green'}
                              onPress={() => navigation.navigate("add_address")}>
                    Add New Address
                </CustomButton>
                {props.data.map((a, i) => <AddressCard
                    {...a}
                    onRemove={onRemove}
                    key={i}/>)}
                <View style={{marginBottom: 200}}/>
            </ScrollView>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        removeAPI: state.address.remove || {},
        data: state.address.all.data || [],
        token: state.profile.token || "",
    };
}

function mapDispatchToProps(dispatch) {
    return {
        removeAddress: (id, token) => dispatch({type: Constants.ADDRESS_REMOVE, payload: {id, token}}),
        getAllAddress: token => dispatch({type: Constants.ADDRESS_GET_ALL, payload: {token}}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MangeAddress);
