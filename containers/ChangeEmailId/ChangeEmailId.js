import React, {useEffect, useState} from 'react';
import styles from "./styles";
import {ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import CustomTextBox from "../../components/CustomTextBox";
import CustomButton from "../../components/CustomButton";
import data from "./data";

const ChangeEmailId = ({navigation, route, ...props}) => {
    let [state, setState] = useState({
        existing: props.profile.email,
        new: "",
    });
    let [error, setError] = useState("");
    useEffect(() => {
        if (state.new.trim().toLowerCase() === state.existing.trim().toLowerCase()) {
            setError(data.same_email)
        } else {
            setError("");
        }
    }, [state.new]);
    return (
        <View>
            <Header left={"BACK"} cart={false} search={false}/>
            <View style={{...styles.appContainer, ...styles.ChangeEmailId}}>
                <Text style={styles.largeTitle}>Change Email Id</Text>
                <CustomTextBox
                    editable={true}
                    label={'Old Email Id'}
                    keyboardType={"email-address"}
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    placeholder={'Existing Mobile'}
                    value={state.existing}
                    onChangeText={() => console.log("")}
                />
                <CustomTextBox
                    editable={false}
                    keyboardType={"email-address"}
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    placeholder={'Enter New Email Id'}
                    value={state.new}
                    error={error !== ""}
                    errorMessage={error}
                    onChangeText={text => setState({...state, new: text})}
                />
                <CustomButton type={'clear'} onPress={() => navigation.navigate("verification_secure")}>
                    Change
                </CustomButton>
            </View>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        profile: state.profile.details || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeEmailId);
