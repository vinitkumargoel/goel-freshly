import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";

const styles = StyleSheet.create({
    ...CommonStyles,
    ChangeEmailId: {
        paddingTop: 50,
    },
    inputBox: {
        marginTop: 12,
        marginHorizontal: 20,
    }
});


export default styles
