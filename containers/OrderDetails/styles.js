import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color} from "../../config/variables";

const styles = StyleSheet.create({
    ...CommonStyles,
    OrderDetails: {},
    status: {
        marginTop: 2,
        fontSize: 15,
        fontWeight: '600',
    },
    infoContainer: {
        marginHorizontal: 20,
        marginVertical: 10,
    },
    contact_us: {
        marginTop: 10,
    },
    date: {
        marginTop: 10,
        fontSize: 18,
        fontWeight: '600',
        marginBottom: 10,
    },
    confirm: {
        color: Color.orange,
    },
    preparing: {
        color: Color.blue,
    },
    delivered: {
        color: Color.green,
    },
    cancelled: {
        color: Color.red,
    },
});


export default styles
