import styles from "./styles";
import {Text} from "react-native";
import React from "react";
import momentTz from "moment-timezone/builds/moment-timezone-with-data";
import {CONTACT_US_MOBILE, RUPEE_SYMBOL} from "../../utils/config";
import moment from "moment";
import {Color, Color2} from "../../config/variables";


export const getTimeLine = data => {
    let final = [];
    for (let i of data.timeline) {
        final.push({
            ...{
                time: moment(i.date).format("DD-MMM, hh:mm"),
                title: i.type,
                ...getDescription(i, data.amount)
            }
        })
    }
    return final;
};

const getDescription = (status, amount) => {
    switch (status.type) {
        case "CREATED":
            return {
                description: "Order Created!",
                circleColor: Color2.Cyan,
                lineColor: Color.orange,
            };
        case "CONFIRMED":
            return {
                description: `${RUPEE_SYMBOL}${amount} Paid and waiting for delivery!`,
                circleColor: Color.orange,
                lineColor: Color.blue,
            };
        case "PREPARING":
            return {
                description: status.message || `Order will deliver soon!`,
                circleColor: Color.blue,
                lineColor: Color.green,
            };
        case "DELIVERED":
            return {
                description: status.message || "Order is Successfully delivered!",
                circleColor: Color.green,
                lineColor: Color.red,
            };
        case "CANCELLED":
            return {
                description: status.message || "Refund Successfully proceed!",
                circleColor: Color.red,
            };
    }
};

export const getStatusMessage = details => {
    const Date = () => <Text style={styles.date}>Order
        Date: {momentTz(details.addedDate).utcOffset("+05:30").format("DD MMM YYYY, HH:mm")}</Text>;
    const contactInfo = (customStyles) => <Text
        style={{...customStyles, ...styles.contact_us}}>Please Contact {CONTACT_US_MOBILE} if you face any
        issue!</Text>;
    switch (details.status.type) {
        case "CONFIRMED":
            return <React.Fragment>
                {Date()}
                <Text style={{...styles.status, ...styles.confirm}}>Amount Paid</Text>
                <Text style={{...styles.status, ...styles.confirm}}>Delivery agent will call you soon!</Text>
                {contactInfo({...styles.status, ...styles.confirm})}
            </React.Fragment>;
        case "PREPARING":
            return <React.Fragment>
                {Date()}
                <Text style={{...styles.status, ...styles.preparing}}>Order Preparing</Text>
                <Text style={{...styles.status, ...styles.preparing}}>Order will deliver soon!</Text>
                {contactInfo({...styles.status, ...styles.preparing})}

            </React.Fragment>;
        case "DELIVERED":
            return <React.Fragment>
                {Date()}
                <Text style={{...styles.status, ...styles.delivered}}>Order Delivered</Text>
                <Text style={{...styles.status, ...styles.delivered}}>Delivered on: </Text>

            </React.Fragment>;
        case "CANCELLED":
            return <React.Fragment>
                {Date()}
                <Text style={{...styles.status, ...styles.cancelled}}>Order Canceled</Text>
                {contactInfo({...styles.status, ...styles.cancelled})}
            </React.Fragment>;
    }
};

