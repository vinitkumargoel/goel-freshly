import React, {useEffect} from 'react';
import styles from "./styles";
import {ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import Constants from "../../middleware/constants";
import isEmpty from "lodash/isEmpty";
import CartTile from "../CartTile";
import CartCalculations from "../../components/CartCalculations";
import {getTimeLine} from "./helper";
import Timeline from 'react-native-timeline-flatlist'
import AddressCard from "../../components/AddressCard";

const OrderDetails = ({navigation, route, ...props}) => {
    useEffect(() => {
        return navigation.addListener('focus', () => {
            props.getOrderDetails(route.params.id, props.token);
        });
    }, [navigation]);
    console.log(props.detail);
    return (
        <View style={styles.OrderDetails}>
            <Header left={"BACK"} title={"Order Details"} cart={false} search={false}/>
            {!isEmpty(props.detail) && <ScrollView style={styles.appContainer}>
                <View style={styles.infoContainer}>
                    <Timeline
                        data={getTimeLine(props.detail)}
                    />
                </View>
                <Text style={styles.largeTitle}>Address</Text>
                <AddressCard removeButton={false} {...props.detail.address}/>
                <Text style={styles.largeTitle}>Products</Text>
                {props.detail.products.map((a, i) => <CartTile key={i}
                                                               productNavigate={false}
                                                               changer={false}
                                                               {...a.product}
                                                               quantity={a.quantity}/>)}
                <CartCalculations data={props.detail.products}/>

                <View style={{marginBottom: 200}}/>
            </ScrollView>}
        </View>
    );
};

function mapStateToProps(state) {
    return {
        detail: state.order.details.data?.data || {},
        token: state.profile.token || "",
    };
}

function mapDispatchToProps(dispatch) {
    return {
        getOrderDetails: (id, token) => dispatch({type: Constants.ORDER_GET_DETAILS, payload: {id, token}}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetails);
