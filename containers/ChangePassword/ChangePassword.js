import React, {useState} from 'react';
import styles from "./styles";
import {ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import CustomTextBox from "../../components/CustomTextBox";
import CustomButton from "../../components/CustomButton";

const ChangePassword = ({navigation, route}) => {
    let [state, setState] = useState({
        new: "",
        confirmNew: "",
    });
    return (
        <View>
            <Header left={"BACK"} cart={false} search={false}/>
            <View style={{...styles.appContainer, ...styles.ChangePassword}}>
                <Text style={styles.largeTitle}>Change Password</Text>
                <CustomTextBox
                    secureTextEntry={true}
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    placeholder={'Enter New Password'}
                    value={state.new}
                    onChangeText={text => setState({...state, new: text})}
                />
                <CustomTextBox
                    secureTextEntry={true}
                    autoCapitalize={'none'}
                    styles={styles.inputBox}
                    placeholder={'Confirm Password'}
                    value={state.confirmNew}
                    onChangeText={text => setState({...state, confirmNew: text})}
                />
                <CustomButton type={'clear'} onPress={() => navigation.navigate("verification_secure")}>
                    Change
                </CustomButton>
            </View>
        </View>
    );
};

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
