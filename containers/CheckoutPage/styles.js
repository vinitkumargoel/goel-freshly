import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color} from "../../config/variables";

const styles = StyleSheet.create({
    ...CommonStyles,
    CheckoutPage: {},
    label: {
        marginHorizontal: 20,
        fontSize: 20,
        fontWeight: '600',
        marginBottom: 10,
    },
    labelAmount: {
        marginHorizontal: 20,
        fontSize: 20,
        fontWeight: '600',
        marginBottom: 10,
        justifyContent: 'center',
    },
    subLabel: {
        width: '50%',
        marginHorizontal: 30,
        fontSize: 36,
        fontWeight: '700',
        marginBottom: 10,
        textAlign: 'right',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        color: Color.darkRed,

    },
    amountContainer: {
        flexDirection: 'row',
        marginRight: 40,
        marginTop: 30,
    }
});


export default styles
