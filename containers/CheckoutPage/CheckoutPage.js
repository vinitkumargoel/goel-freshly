import React, {useEffect, useState} from 'react';
import styles from "./styles";
import {ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import Constants from "../../middleware/constants";
import AddressCard from "../../components/AddressCard";
import CustomButton from "../../components/CustomButton";
import isEmpty from "lodash/isEmpty";
import calculateCartValue from "../../utils/calculateCartValue";
import RazorpayCheckout from 'react-native-razorpay';
import paymentGateway from "../../utils/paymentGateway";
import OrderFailed from "../../components/OrderMessages/OrderFailed";
import {RUPEE_SYMBOL} from "../../utils/config";

const CheckoutPage = ({navigation, route, ...props}) => {
    useEffect(() => {
        return navigation.addListener('focus', () => {
            props.getAllAddress(props.token);
        });
    }, [navigation]);
    let {totalPayable} = calculateCartValue(props.cartData);
    let [status, setStatus] = useState({message: "", code: ""});
    useEffect(() => {
        if (!isEmpty(props.createOrderAPI.data)) {
            props.resetCart();
            props.resetOrder();
            navigation.navigate("order_success", {...props.createOrderAPI.data});
        }

    }, [props.createOrderAPI]);
    useEffect(() => {
        if (isEmpty(props.checkoutData.address)) {
            props.setAddress(props.allAddress.data[0])
        }
    }, [props.allAddress.data]);
    return (
        <View style={styles.CheckoutPage}>
            <Header left={"BACK"} title={"Checkout"} search={false}/>
            <ScrollView style={styles.appContainer}>
                <Text style={styles.label}>Delivery Address</Text>
                {!props.allAddress.loading && props.allAddress.data.length === 0 &&
                <CustomButton icon={'plus'}
                              style={styles.checkoutButton}
                              type={'green'}
                              onPress={() => navigation.navigate("add_address_2")}>
                    Add New Address
                </CustomButton>}
                {!props.allAddress.loading && props.allAddress.data.length !== 0 &&
                <AddressCard onPress={() => navigation.navigate("select_address")} {...props.checkoutData.address}
                             removeButton={false}/>}
                <View style={styles.amountContainer}>
                    <Text style={styles.labelAmount}>Total Amount</Text>
                    <Text style={styles.subLabel}>{RUPEE_SYMBOL}{totalPayable}</Text>
                </View>
                <CustomButton icon={'money-bill'}
                              style={styles.checkoutButton}
                              type={'green'}
                              onPress={() => {
                                  const options = paymentGateway.config(props.profile, totalPayable);
                                  RazorpayCheckout.open(options).then((data) => {
                                      let body = paymentGateway.finalOrderData(props.checkoutData, props.cartData, data["razorpay_payment_id"]);
                                      props.createOrder(body, props.token);
                                  }).catch((error) => {
                                      setStatus({code: "FAILED", message: error});
                                  });
                              }}>
                    Proceed For Payment
                </CustomButton>
                {status.code === "FAILED" &&
                <OrderFailed onPress={() => setStatus({code: "", message: ""})} data={status.message}/>}
            </ScrollView>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        createOrderAPI: state.order.create || {},
        checkoutData: state.checkout || {},
        allAddress: state.address.all || [],
        profile: state.profile.details || {},
        token: state.profile.token || "",
        cartData: state.cart.detailedProduct.data || [],
    };
}

function mapDispatchToProps(dispatch) {
    return {
        resetOrder: () => dispatch({type: Constants.ORDER_CREATE_RESET}),
        resetCart: () => dispatch({type: Constants.CART_RESET}),
        createOrder: (body, token) => dispatch({type: Constants.ORDER_CREATE, payload: {body, token}}),
        setAmount: amount => dispatch({type: Constants.CHECKOUT_SET_AMOUNT, payload: amount}),
        setAddress: address => dispatch({type: Constants.CHECKOUT_SET_ADDRESS, payload: address}),
        getAllAddress: token => dispatch({type: Constants.ADDRESS_GET_ALL, payload: {token}}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPage);
