import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";

const styles = StyleSheet.create({
    ...CommonStyles,
    VerificationCodeScreen: {
        marginTop: 150,
    },
    title: {
        fontSize: 30,
        fontWeight: '700',
        paddingTop: 5,
        textAlign: 'center',
        marginBottom: 10,
    },
    subtitle: {
        textAlign: 'center',
    },
    CodeContainer: {
        marginTop: 40,
        flexDirection: 'row',
        alignSelf: 'center',
        alignContent: 'center',
    },
    inputBox: {
        width: 50,
        fontSize: 30,
        fontWeight: '700',
        textAlign: 'center',
        marginHorizontal: 10,
    },
    button: {
        marginTop: 30,
        marginHorizontal: 40
    }
});


export default styles
