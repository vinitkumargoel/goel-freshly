import React, {useState} from 'react';
import {Text, View} from "react-native";
import styles from "./styles";
import {connect} from "react-redux";
import CustomTextBox from "../../components/CustomTextBox";
import CustomButton from "../../components/CustomButton";
import VerificationInput from "../../components/VerificationInput";

const VerificationCodeScreen = ({navigation}) => {
    let [state, setState] = useState({
        v0: "",
        v1: "",
        v2: "",
        v3: "",
    });
    return (
        <View style={styles.VerificationCodeScreen}>
            <Text style={styles.title}>Verification Code</Text>
            <Text style={styles.subtitle}>OTP is sent on your mobile.</Text>
            <View style={styles.CodeContainer}>
                <VerificationInput/>
            </View>
            <CustomButton style={styles.button}
                          type={'clear'}
                          onPress={() => navigation.navigate("Home")}>
                Next
            </CustomButton>
            <CustomButton icon={'long-arrow-alt-left'} style={styles.button} type={'clean'} onPress={() => navigation.navigate("register")}>
                Change Mobile Number
            </CustomButton>
        </View>
    );
};

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(VerificationCodeScreen);
