import React from "react";

import LandingPage from "./LandingPage";
import createStackNavigator from "@react-navigation/stack/src/navigators/createStackNavigator";
import ProductDetails from "../ProductDetails";
import ProductSeeAll from "../ProductSeeAll";
import CartPage from "../CartPage";
import ProfilePage from "../ProfilePage";
import OrderPage from "../OrderPage";
import SettingsPage from "../SettingsPage";
import CheckoutPage from "../CheckoutPage";
import SelectAddress from "../SelectAddress";
import AddAddressPage from "../AddAddressPage";
import {OrderSuccess} from "../../components/OrderMessages";
import OrderDetails from "../OrderDetails";

const Stack = createStackNavigator();

const Component = ({navigation, route}) => {
    navigation.setOptions({
        headerMode: 'none',
    });
    return (
        <Stack.Navigator
            headerMode='none'
            screenOptions={{}}>
            <Stack.Screen
                name="Home"
                initialParams={{navigation, route}}
                component={LandingPage}
                options={{title: 'Home', headerMode: 'none',}}
            />
            <Stack.Screen
                name="product_details"
                initialParams={{navigation, route}}
                component={ProductDetails}
                options={{title: 'Product Details', headerMode: 'none',}}
            />
            <Stack.Screen
                name="product_see_all"
                initialParams={{navigation, route}}
                component={ProductSeeAll}
                options={{title: 'Product SeeAll', headerMode: 'none',}}
            />
            <Stack.Screen
                name="cart_page"
                initialParams={{navigation, route}}
                component={CartPage}
                options={{title: 'Cart', headerMode: 'none',}}
            />
            <Stack.Screen
                name="profile"
                initialParams={{navigation, route}}
                component={ProfilePage}
                options={{title: 'Profile', headerMode: 'none',}}
            />
            <Stack.Screen
                name="orders"
                initialParams={{navigation, route}}
                component={OrderPage}
                options={{title: 'Orders', headerMode: 'none',}}
            />
            <Stack.Screen
                name="settings"
                initialParams={{navigation, route}}
                component={SettingsPage}
                options={{title: 'Settings', headerMode: 'none',}}
            />
            <Stack.Screen
                name="checkout"
                initialParams={{navigation, route}}
                component={CheckoutPage}
                options={{title: 'checkout', headerMode: 'none',}}
            />
            <Stack.Screen
                name="select_address"
                initialParams={{navigation, route}}
                component={SelectAddress}
                options={{title: 'add_address', headerMode: 'none',}}
            />
            <Stack.Screen
                name="add_address_2"
                initialParams={{navigation, route}}
                component={AddAddressPage}
                options={{title: 'add_address', headerMode: 'none',}}
            />
            <Stack.Screen
                name="order_success"
                initialParams={{navigation, route}}
                component={OrderSuccess}
                options={{title: 'order_success', gestureEnabled: false, headerMode: 'none',}}
            />
            <Stack.Screen
                name="order_details"
                initialParams={{navigation, route}}
                component={OrderDetails}
                options={{title: 'order_details', headerMode: 'none',}}
            />
        </Stack.Navigator>
    )
};
export default Component;
