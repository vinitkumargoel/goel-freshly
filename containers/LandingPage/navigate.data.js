export const VEGETABLES = [{
    "_id": "5ea07ba3c77d8cdd4972bfbf",
    "level": 0,
    "title": "Fresh",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfc1",
    "level": 1,
    "title": "Vegetables",
    "parentId": "5ea07ba3c77d8cdd4972bfbf",
}];


export const FRUITS = [{
    "_id": "5ea07ba3c77d8cdd4972bfbf",
    "level": 0,
    "title": "Fresh",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfc0",
    "level": 1,
    "title": "Fruits",
    "parentId": "5ea07ba3c77d8cdd4972bfbf",
}];

export const DAIRY_PRODUCTS = [{
    "_id": "5ea07ba3c77d8cdd4972bfd0",
    "level": 0,
    "title": "Dairy Goodness",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfd1",
    "level": 1,
    "title": "Milk",
    "parentId": "5ea07ba3c77d8cdd4972bfd0"
}];

export const BREADS = [{
    "_id": "5ea07ba3c77d8cdd4972bfc5",
    "level": 0,
    "title": "Breakfast",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfc6",
    "level": 1,
    "title": "Bread",
    "parentId": "5ea07ba3c77d8cdd4972bfc5",
}];

export const EGGS = [{
    "_id": "5ea07ba3c77d8cdd4972bfc5",
    "level": 0,
    "title": "Breakfast",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfc7",
    "level": 1,
    "title": "Eggs",
    "parentId": "5ea07ba3c77d8cdd4972bfc5",
}];

export const ORAL_CARE = [{
    "_id": "5ea07ba3c77d8cdd4972bfec",
    "level": 0,
    "title": "Personal Care",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfed",
    "level": 1,
    "title": "Oral Care",
    "parentId": "5ea07ba3c77d8cdd4972bfec",
}];


export const BATH_BODY = [{
    "_id": "5ea07ba3c77d8cdd4972bfec",
    "level": 0,
    "title": "Personal Care",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfee",
    "level": 1,
    "title": "Bath & Body",
    "parentId": "5ea07ba3c77d8cdd4972bfec",
}];

export const FACE_SKIN = [{
    "_id": "5ea07ba3c77d8cdd4972bfec",
    "level": 0,
    "title": "Personal Care",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfef",
    "level": 1,
    "title": "Face & Skin",
    "parentId": "5ea07ba3c77d8cdd4972bfec",
}];


export const GROOMING = [{
    "_id": "5ea07ba3c77d8cdd4972bfec",
    "level": 0,
    "title": "Personal Care",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bff0",
    "level": 1,
    "title": "Grooming",
    "parentId": "5ea07ba3c77d8cdd4972bfec",
}];

export const HAIR_CARE = [{
    "_id": "5ea07ba3c77d8cdd4972bfec",
    "level": 0,
    "title": "Personal Care",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bff1",
    "level": 1,
    "title": "Hair Care",
    "parentId": "5ea07ba3c77d8cdd4972bfec",
}];

export const FEMININE_CARE = [{
    "_id": "5ea07ba3c77d8cdd4972bfec",
    "level": 0,
    "title": "Personal Care",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bff3",
    "level": 1,
    "title": "Faeminie Care",
    "parentId": "5ea07ba3c77d8cdd4972bfec",
}];

export const SHAVING = [{
    "_id": "5ea07ba3c77d8cdd4972bfec",
    "level": 0,
    "title": "Personal Care",
    "parentId": null,
}, {
    "_id": "5ea13e64131680048f1cb243",
    "level": 1,
    "title": "Shaving",
    "parentId": "5ea07ba3c77d8cdd4972bfec"
}];


export const TOILET_BATHROOM = [{
    "_id": "5ea07ba3c77d8cdd4972bfe6",
    "level": 0,
    "title": "Cleaning Essentials",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfea",
    "level": 1,
    "title": "Cleaners",
    "parentId": "5ea07ba3c77d8cdd4972bfe6",
}];


export const SURFACE_CLEANER = [{
    "_id": "5ea07ba3c77d8cdd4972bfe6",
    "level": 0,
    "title": "Cleaning Essentials",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfea",
    "level": 1,
    "title": "Cleaners",
    "parentId": "5ea07ba3c77d8cdd4972bfe6",
}];

export const REPELLENTS = [{
    "_id": "5ea07ba3c77d8cdd4972c000",
    "level": 0,
    "title": "Home Essentials",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972c002",
    "level": 1,
    "title": "Repellents",
    "parentId": "5ea07ba3c77d8cdd4972c000",
}];

export const LAUNDRY = [{
    "_id": "5ea07ba3c77d8cdd4972bfe6",
    "level": 0,
    "title": "Cleaning Essentials",
    "parentId": null,
}, {
    "_id": "5ea07ba3c77d8cdd4972bfeb",
    "level": 1,
    "title": "Detergents",
    "parentId": "5ea07ba3c77d8cdd4972bfe6",
}];
