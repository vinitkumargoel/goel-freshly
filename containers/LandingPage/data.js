import {
    BATH_BODY,
    BREADS,
    DAIRY_PRODUCTS,
    EGGS,
    FACE_SKIN,
    FEMININE_CARE,
    FRUITS,
    GROOMING,
    HAIR_CARE,
    LAUNDRY,
    ORAL_CARE,
    REPELLENTS,
    SHAVING,
    SURFACE_CLEANER,
    TOILET_BATHROOM,
    VEGETABLES
} from "./navigate.data";

export const FRESH_DATA = {
    label: "Fresh",
    data: [{
        label: "Vegetable",
        image: require("../../assets/images/LandingPage/diet.png"),
        data: VEGETABLES
    }, {
        label: "Fruits",
        image: require("../../assets/images/LandingPage/harvest.png"),
        data: FRUITS
    }, {
        label: "Dairy Products",
        image: require("../../assets/images/LandingPage/milk.png"),
        data: DAIRY_PRODUCTS
    }, {
        label: "Bread",
        image: require("../../assets/images/LandingPage/bread.png"),
        data: BREADS
    }, {
        label: "Eggs",
        image: require("../../assets/images/LandingPage/egg.png"),
        data: EGGS
    }],
};


export const HomeCardData = {
    label: "Hygiene Essentials!",
    subLabel: "For Healthy Homes!",
    data: [{
        label: "Toilet & Bathroom Cleaners",
        image: require("../../assets/images/LandingPage/harpic-toilet-cleaner.png"),
        data: TOILET_BATHROOM,
    }, {
        label: "Surface Cleaners",
        image: require("../../assets/images/LandingPage/surface-cleaners.png"),
        data: SURFACE_CLEANER,
    }, {
        label: "Repellents",
        image: require("../../assets/images/LandingPage/Repellents.png"),
        data: REPELLENTS,
    }, {
        label: "Laundry & Dishwasher",
        image: require("../../assets/images/LandingPage/laundry_dishwasher.png"),
        data: LAUNDRY,
    }],
};

export const PERSONAL_CARE = {
    label: "Personal Care",
    data: [{
        label: "Oral Care",
        image: require("../../assets/images/LandingPage/toothbrush.png"),
        data: ORAL_CARE
    }, {
        label: "Bath & Body",
        image: require("../../assets/images/LandingPage/soap.png"),
        data: BATH_BODY
    }, {
        label: "Face & Skin",
        image: require("../../assets/images/LandingPage/skincare.png"),
        data: FACE_SKIN,
    }, {
        label: "Grooming",
        image: require("../../assets/images/LandingPage/beauty-salon.png"),
        data: GROOMING
    }, {
        label: "Hair Care",
        image: require("../../assets/images/LandingPage/shampoo.png"),
        data: HAIR_CARE
    }, {
        label: "Feminine Care",
        image: require("../../assets/images/LandingPage/women-pad.png"),
        data: FEMININE_CARE
    }, {
        label: "Shaving",
        image: require("../../assets/images/LandingPage/shaving.png"),
        data: SHAVING
    }],
};
