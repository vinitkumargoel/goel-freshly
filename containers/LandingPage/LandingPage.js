import React, {useEffect} from 'react';
import styles from "./styles";
import {ScrollView, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import IconSlide from "../../components/IconSlide";
import {FRESH_DATA, HomeCardData, PERSONAL_CARE} from "./data";
import HomeCard from "../../components/HomeCard";
import HomeProductContainer from "../HomeProductContainer";
import Constants from "../../middleware/constants";

const LandingPage = ({navigation, route, ...props}) => {
    useEffect(() => {
        return navigation.addListener('focus', () => {
            props.clearFilter();
        });
    }, [navigation]);
    const navigate = ([l1, l2]) => {
        props.changeCategory({l1, l2});
        navigation.navigate("product_see_all")
    };
    return (
        <View style={styles.LandingPage}>
            <Header left={"MENU"}/>
            <ScrollView style={styles.appContainer}>
                <IconSlide {...FRESH_DATA} navigate={navigate}/>
                <HomeCard {...HomeCardData} navigate={navigate}/>
                <IconSlide {...PERSONAL_CARE} navigate={navigate}/>
                <HomeProductContainer label={"Popular Items"} navigate={navigate}/>
            </ScrollView>
        </View>
    );
};

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return {
        clearFilter: () => dispatch({type: Constants.SEE_ALL_CLEAR_FILTERS}),
        changeCategory: payload => dispatch({type: Constants.SEE_ALL_CHANGE_CATEGORY, payload}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);
