import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import utils from "../../config/utils";
import Colors from "../../constants/Colors";

const columnSize = utils.columnSizeCustom(30);
const BUTTON_SIZE = 30;
const styles = StyleSheet.create({
    ...CommonStyles,
    CartTile: {
        borderWidth: 1,
        borderColor: "#ececec",
        borderRadius: 10,
        marginBottom: 20,
        padding: 10,
        flexDirection: 'row',
    },
    imageContainer: {
        paddingRight: 10,

        width: columnSize * 8,
    },
    image: {
        width: '100%',
        height: 120,
        marginRight: 10,
        resizeMode: 'stretch',
        marginBottom: 10,
    },
    infoContainer: {
        paddingTop: 10,
        width: columnSize * 16,
    },
    title: {
        fontSize: 16,
    },
    PriceContainer: {
        flexDirection: 'row',
    },
    Price: sale => {
        return {
            textDecorationLine: sale ? 'line-through' : 'none',
            color: Colors.cartPrize,
            marginBottom: 20,
            fontSize: 15,
            fontWeight: "bold",
        }
    },
    Sale: {
        marginLeft: 10,
        fontSize: 14,
        lineHeight: 20,
        color: Colors.cartPrize,
        fontWeight: "bold",
    },
    ActionsContainer: {
        flexDirection: 'row',
    },
    QuantityChangeContainer: {
        width: '40%',
        flexDirection: 'row',
    },
    QuantityChangeButton: {
        width: BUTTON_SIZE,
        height: BUTTON_SIZE,
        borderRadius: BUTTON_SIZE / 2,
        padding: 8,
        borderWidth: 1,
        borderColor: Colors.buttonBorder,
    },
    QuantityChangeText: {
        paddingVertical: 8,
        marginHorizontal: 10,
    },
    TotalPrizeContainer: changer => {
        return {
            alignContent: 'flex-end',
            width: changer ? '60%' : "100%",
        }
    },
    TotalPrice: {
        fontSize: 24,
        color: Colors.cartTotalPrize,
        fontWeight: "bold",
        alignContent: 'flex-end',
        textAlign: 'right',
    },
});


export default styles
