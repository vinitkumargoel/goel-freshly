import React from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import utils from "../../config/utils";
import {FontAwesome5} from '@expo/vector-icons';
import {connect} from "react-redux";
import Constants from "../../middleware/constants";
import {MAX_PRODUCT_LIMIT, RUPEE_SYMBOL} from "../../utils/config";
import {useNavigation} from '@react-navigation/native';

const CartTile = ({
                      _id,
                      title = "",
                      image = [],
                      price = {
                          regular: 0,
                          sale: 0,
                      },
                      quantity,
                      changer = true,
                      productNavigate = true,
                      ...props
                  }) => {
    let isSale = utils.isSaleProduct(price);
    let navigation = useNavigation();
    return (
        <View style={styles.CartTile}>
            <View style={styles.imageContainer}>
                <Image source={{uri: image[0]}} style={styles.image}/>
            </View>
            <View style={styles.infoContainer}>
                <TouchableOpacity
                    onPress={() => productNavigate && navigation.navigate('product_details', {id: _id})}><Text
                    style={styles.title}>{title}</Text></TouchableOpacity>
                <View style={styles.PriceContainer}>
                    <Text style={styles.Price(isSale)}>{RUPEE_SYMBOL}{price.regular}</Text>
                    {isSale && <Text style={styles.Sale}>{RUPEE_SYMBOL}{price.sale}</Text>}
                </View>
                <View style={styles.ActionsContainer}>

                    {changer && <View style={styles.QuantityChangeContainer}>
                        {quantity !== 1 &&
                        <TouchableOpacity onPress={() => props.changeQuantity({id: _id, quantity: --quantity})}
                                          style={styles.QuantityChangeButton}>
                            <FontAwesome5 name={'minus'}/>
                        </TouchableOpacity>}
                        {quantity === 1 &&
                        <TouchableOpacity onPress={() => props.removeProduct(_id)} style={styles.QuantityChangeButton}>
                            <FontAwesome5 name={'trash-alt'}/>
                        </TouchableOpacity>}
                        <Text style={styles.QuantityChangeText}>{quantity}</Text>
                        {quantity !== MAX_PRODUCT_LIMIT &&
                        <TouchableOpacity onPress={() => props.changeQuantity({id: _id, quantity: ++quantity})}
                                          style={styles.QuantityChangeButton}>
                            <FontAwesome5 name={'plus'}/>
                        </TouchableOpacity>}
                    </View>}
                    <View style={styles.TotalPrizeContainer(changer)}>
                        <Text
                            style={styles.TotalPrice}>{RUPEE_SYMBOL}{isSale ? price.sale * quantity : price.regular * quantity}</Text>
                    </View>
                </View>

            </View>
        </View>
    );
};

function mapStateToProps(state) {
    return state;
}

function mapDispatchToProps(dispatch) {
    return {
        removeProduct: id => dispatch({type: Constants.REMOVE_FROM_CART, payload: id}),
        changeQuantity: payload => dispatch({type: Constants.CHANGE_PRODUCT_QUANTITY, payload})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartTile);
