import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color} from "../../config/variables";

const styles = StyleSheet.create({
    ...CommonStyles,
    ProductDetails: {
        // paddingBottom: 70,
    },
    Image: {
        width: '100%',
        height: 400,
        resizeMode: 'stretch',
    },
    brandName: {
        marginTop: 20,
        fontSize: 13,
        fontWeight: "300",
    },
    title: {
        marginBottom: 10,
        fontSize: 20,
        fontWeight: "bold",
    },
    PriceContainer: {
        flexDirection: 'row',
    },
    Price: sale => {
        return {
            textDecorationLine: sale ? 'line-through' : 'none',
            color: Color.darkRed,
            marginBottom: 20,
            fontSize: 20,
            fontWeight: "bold",
        }
    },
    Sale: {
        marginLeft: 10,
        fontSize: 18,
        lineHeight: 26,
        color: Color.darkRed,
        fontWeight: "bold",
    },
    subTitle: {
        marginBottom: 10,
        fontSize: 16,
        fontWeight: "bold",
    },
    descriptionContainer: {
        marginTop: 20,
        marginBottom: 30,
    },
    description: {
        justifyContent: 'space-around'
    },
    link: {
        color: Color.darkblue
    },
    infoContainer: {
        marginBottom: 200
    },
    infoTile: {},
    addCartButton: {},
});


export default styles
