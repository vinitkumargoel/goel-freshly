import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";

const Description = ({description}) => {
    let [more, setMore] = useState(false);
    let upperLimit = more ? description.length : 300;
    return (
        <View style={styles.descriptionContainer}>
            <Text style={styles.subTitle}>Description</Text>
            <Text style={styles.description}>{description?.substring(0, upperLimit)}</Text>
            {more ?
                <TouchableOpacity onPress={() => setMore(false)}>
                    <Text style={styles.link}>Read Less</Text>
                </TouchableOpacity> :
                <TouchableOpacity onPress={() => setMore(true)}>
                    <Text style={styles.link}>Read More</Text>
                </TouchableOpacity>}
        </View>
    );
};

Description.propTypes = {
    description: PropTypes.string,
};

export default Description;
