import isEmpty from "lodash/isEmpty";
import {Color} from "../../config/variables";

export const isAlreadyOnCart = (cartData = [], id) => {
    let find = cartData.find((a) => a.id === id);
    return !isEmpty(find);
};

export const ICON_COLOR = (type, color) => {
    switch (type) {
        case 'green':
            return Color.white;
        case 'clear':
            return Color.black;
        case 'red':
            return Color.white;
        case 'clean':
            return color ? color : Color.black;
        default:
            return Color.black;
    }
};

export const ICON_BG = (type) => {
    switch (type) {
        case 'green':
            return Color.green;
        case 'clear':
            return 'transparent';
        case 'red':
            return Color.red;
        case 'clean':
            return 'transparent';
        default:
            return Color.white;
    }
};
