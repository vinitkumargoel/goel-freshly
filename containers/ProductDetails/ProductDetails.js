import React, {useEffect} from 'react';
import {ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import Header from "../Header";
import ImageSlideShow from "./ImageSlideShow";
import styles from "./styles";
import Description from "./Description";
import Info from "./Info";
import Constants from "../../middleware/constants";
import {isAlreadyOnCart} from "./helper";
import utils from "../../config/utils";
import CustomButton from "../../components/CustomButton";
import {RUPEE_SYMBOL} from "../../utils/config";

const ProductDetails = props => {
    let {product, cartData = [], token} = props;
    useEffect(() => {
        props.getProductDetails(props.route?.params?.id, token);
    }, []);
    let isSale = utils.isSaleProduct(product?.price);
    return (
        <View style={styles.ProductDetails}>
            <Header left={"BACK"} search={false}/>
            <ScrollView style={styles.appContainer}>
                <ImageSlideShow image={product?.image ?? []}/>
                <Text style={styles.brandName}>{product.brand?.name}</Text>
                <Text style={styles.title}>{product.title}</Text>
                <View style={styles.PriceContainer}>
                    <Text style={styles.Price(isSale)}>{RUPEE_SYMBOL}{product?.price?.regular}</Text>
                    {isSale && <Text style={styles.Sale}>{RUPEE_SYMBOL}{product?.price?.sale}</Text>}
                </View>
                {!isAlreadyOnCart(cartData, product?._id) && <CustomButton
                    icon={'cart-plus'}
                    onPress={() => props.addProductToCart(product)}
                    type={'green'}>
                    Add to Cart
                </CustomButton>}
                {isAlreadyOnCart(cartData, product?._id) && <CustomButton
                    icon={'cart-arrow-down'}
                    onPress={() => props.removeProductFromCart(product?._id)}
                    type={'red'}>
                    Remove from Cart
                </CustomButton>}
                {product?.description ?? false ? <Description description={product?.description}/> : null}
                {product?.info ?? false ? <Info data={product.info}/> : null}
            </ScrollView>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        token: state.profile.token || "",
        cartData: state.cart.detailedProduct.data || [],
        product: state.product.details.data || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        removeProductFromCart: id => dispatch({type: Constants.REMOVE_FROM_CART, payload: id}),
        getProductDetails: (id, token) => dispatch({type: Constants.GET_PRODUCT_DETAILS, payload: {id, token}}),
        addProductToCart: payload => dispatch({type: Constants.ADD_IN_CART, payload}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
