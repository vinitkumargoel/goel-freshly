import React from 'react';
import PropTypes from 'prop-types';
import {Text, View} from "react-native";
import styles from "./styles";

const Info = ({data}) => {
    return (
        <View style={styles.infoContainer}>
            <Text style={styles.subTitle}>Info</Text>
            <Text style={styles.infoTile}>{data}</Text>
        </View>
    );
};

Info.propTypes = {
    data: PropTypes.string,
};

export default Info;
