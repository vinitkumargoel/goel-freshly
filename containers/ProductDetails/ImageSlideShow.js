import React from 'react';
import PropTypes from 'prop-types';
import {Image} from "react-native";
import styles from "./styles";
import {Carousel} from "@ant-design/react-native";

const ImageSlideShow = props => {
    return (
        <Carousel autoplay={false}>
            {props.image.map((a, i) => (
                <Image
                    key={i}
                    source={{uri: a}}
                    alt=""
                    style={styles.Image}
                />
            ))}
        </Carousel>
    );
};

ImageSlideShow.propTypes = {
    image: PropTypes.array.isRequired,
};

export default ImageSlideShow;
