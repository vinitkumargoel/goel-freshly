
# Android Key generate

keytool -genkey -v -keystore android-package.keystore -alias freshly -keyalg RSA -keysize 2048 -validity 10000

* Password: qwerty12345
* Name: Vinit
* Organization Unit: Goel Freshly
* Organization: Goel Freshly
* City: Bangalore
* State: Karnataka
* Country code: IN

#### Commands

cd android && ./gradlew assembleRelease
