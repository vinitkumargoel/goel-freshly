import {StyleSheet} from "react-native";
import {Color, MAIN_PADDING} from "./variables";

export default StyleSheet.create({
    container: {
        display: 'flex',
    },
    appContainer: {
        padding: MAIN_PADDING,
        backgroundColor: Color.white,
        marginBottom: 80,
        height: '100%',
    },
    largeTitle: {
        fontSize: 30,
        fontWeight: '700',
        paddingTop: 5,
        textAlign: 'center',
        marginBottom: 40,
    }
});
