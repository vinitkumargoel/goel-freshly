import {Dimensions} from 'react-native';
import {MAIN_PADDING} from "./variables";

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default {
    windowWidth,
    windowHeight,
    columnSize: (windowWidth - (MAIN_PADDING * 2)) / 24,
    columnSizeCustom: size => (windowWidth - (MAIN_PADDING * 2) - size) / 24,
    isSaleProduct: (price = {}) => price.sale !== 0 && price.sale !== price.regular,
    isEmail: email => /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email),
}
