export const MAIN_PADDING = 20;
export const Color = {
    black: 'black',
    white: 'white',
    darkRed: 'darkred',
    blue: '#007bff',
    darkblue: 'darkblue',
    lightblue: 'lightblue',
    green: '#25AF3B',
    orange: '#FF6600',
    red: '#dc3545',
    grey1: "#3a3939",
    grey2: "#ececec",
    grey3: "#444444",
    grey4: "#ebebed",
    grey5: "#b7b7b7",
};

export const Color2 = {
    Primary: "#20a8d8",
    Secondary: "#c8ced3",
    Success: "#4dbd74",
    Danger: "#f86c6b",
    Warning: "#ffc107",
    Info: "#63c2de",
    Light: "#f0f3f5",
    Dark: "#2f353a",

    //GREY
    Gray1: "#f0f3f5",
    Gray2: "#e4e7ea",
    Gray3: "#c8ced3",
    Gray4: "#acb4bc",
    Gray5: "#8f9ba6",
    Gray6: "#73818f",
    Gray7: "#5c6873",
    Gray8: "#2f353a",
    Gray9: "#23282c",

    //Additional Colors
    Blue: "#20a8d8",
    LightBlue: "#63c2de",
    Indigo: "#6610f2",
    Purple: "#6f42c1",
    Pink: "#e83e8c",
    Red: "#f86c6b",
    Orange: "#f8cb00",
    Yellow: "#ffc107",
    Green: "#4dbd74",
    Teal: "#20c997",
    Cyan: "#17a2b8",
};

export const TOP_PADDING = 50;
