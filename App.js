import * as React from 'react';
import {useEffect} from 'react';
import {Platform, StatusBar, StyleSheet, View} from 'react-native';
import {SplashScreen} from 'expo';
import * as Font from 'expo-font';
import {Ionicons} from '@expo/vector-icons';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import useLinking from './navigation/useLinking';
import {Provider} from "@ant-design/react-native";
import AppDrawer from "./navigation/DrawerNavigator";
import {connect, Provider as ReduxProvider} from 'react-redux';
import store from "./middleware/store";
import Constants from "./middleware/constants";

const Stack = createStackNavigator();
console.disableYellowBox = true;
export default function App(props) {
    const [isLoadingComplete, setLoadingComplete] = React.useState(false);
    const [initialNavigationState, setInitialNavigationState] = React.useState();
    const containerRef = React.useRef();

    // Load any resources or data that we need prior to rendering the app
    React.useEffect(() => {
        async function loadResourcesAndDataAsync() {
            try {
                SplashScreen.preventAutoHide();

                // Load our initial navigation state

                // Load fonts
                await Font.loadAsync({
                    ...Ionicons.font,
                    'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
                });
                await Font.loadAsync(
                    'antoutline',
                    require('@ant-design/icons-react-native/fonts/antoutline.ttf')
                );

                await Font.loadAsync(
                    'antfill',
                    // eslint-disable-next-line
                    require('@ant-design/icons-react-native/fonts/antfill.ttf')
                );
            } catch (e) {
                // We might want to provide this error information to an error reporting service
                console.warn(e);
            } finally {
                setLoadingComplete(true);
                SplashScreen.hide();
            }
        }

        loadResourcesAndDataAsync().then(r => {});
    }, []);

    if (!isLoadingComplete && !props.skipLoadingScreen) {
        return <View/>;
    } else {
        return (
            <View style={styles.container}>
                <ReduxProvider store={store}>
                    <Provider>
                        {Platform.OS === 'ios' && <StatusBar barStyle="default"/>}
                        <ConnectedMainComponent containerRef={containerRef}
                                                initialNavigationState={initialNavigationState}/>

                    </Provider>
                </ReduxProvider>
            </View>
        );
    }
}

const MainComponent = ({containerRef, initialNavigationState, ...props}) => {
    useEffect(() => {
        props.getCart();
        props.getAllCategory();
        props.getToken();
    }, []);

    useEffect(() => {
        if (props.token) {
            props.verifyToken(props.token);
        }
    }, [props.token]);
    return (
        <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
            <Stack.Navigator>
                <Stack.Screen name="Root" component={AppDrawer}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
};

function mapStateToProps(state) {
    return {
        token: state.profile.token || "",
        checkToken: state.auth.checkToken || {},
    };
}

function mapDispatchToProps(dispatch) {
    return {
        verifyToken: token => dispatch({type: Constants.AUTH_CHECK_TOKEN, payload: {token}}),
        getCart: () => dispatch({type: Constants.GET_ALL_CART_DETAILED_PRODUCTS}),
        getToken: () => dispatch({type: Constants.PROFILE_GET_TOKEN}),
        getAllCategory: () => dispatch({type: Constants.CATEGORY_GET_ALL}),
    }
}

let ConnectedMainComponent = connect(mapStateToProps, mapDispatchToProps)(MainComponent);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});
