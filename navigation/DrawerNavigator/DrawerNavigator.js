import {createDrawerNavigator} from '@react-navigation/drawer';
import LandingPage from "../../containers/LandingPage";
import React from "react";
import DrawerScreen from "../../containers/DrawerScreen/DrawerScreen";
import LoginScreen from "../../containers/LoginScreen";

const Drawer = createDrawerNavigator();

function AppDrawer({navigation}) {
    navigation.setOptions({headerShown: false});
    return (
        <Drawer.Navigator drawerContent={(props) => <DrawerScreen {...props}/>}>
            <Drawer.Screen name="Login" component={LoginScreen}/>
            <Drawer.Screen name="Home" component={LandingPage}/>
        </Drawer.Navigator>
    );
}

export default AppDrawer
