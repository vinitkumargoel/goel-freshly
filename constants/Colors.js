import {Color} from "../config/variables";

const tintColor = '#2f95dc';

export default {
    tintColor,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColor,
    tabBar: '#fefefe',
    errorBackground: 'red',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',
    buttonBorder: "#bbbbbb",
    cartPrize: Color.black,
    cartTotalPrize: Color.darkRed,
    cartCalLabel: "#3a3939",
    cartCalLabelBig: Color.black,
    cartCalValue: "#3a3939",
    cartCalValueFocused: "#33c1cb",
    cartCalValueBig: Color.black,
    drawerMainText: Color.darkRed,
    verticalTabBorder: "#ebebed",
};
