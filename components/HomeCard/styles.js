import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import utils from "../../config/utils";

const styles = StyleSheet.create({
    ...CommonStyles,
    HomeCard: {
        marginBottom:20,
    },
    HomeCardLabel: {
        fontSize: 20,
        fontWeight: "bold"
    },
    HomeCardSubLabel: {
        fontSize: 13,
        fontWeight: "bold",
        lineHeight: 23,
    },
    CardContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    Card: {
        width: utils.columnSize * 11,
        alignItems: 'center',
        marginVertical: 10,
        backgroundColor: '#cbf1f4',
        padding: 20,
        marginLeft: 10,
        borderRadius: 10,
    },
    CardLabel: {
        paddingTop: 5,
        fontSize: 12,
        fontWeight: "700",
    },
    CardImage: {
        width: 90,
        height: 130,
        resizeMode: 'stretch',
    },
});


export default styles
