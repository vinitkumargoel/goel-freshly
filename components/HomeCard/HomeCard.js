import React from 'react';
import PropTypes from 'prop-types';
import {Text, View} from "react-native";
import styles from "./styles";
import Card from "./Card";

const HomeCard = ({
                      label = "",
                      subLabel = "",
                      data = [],
                      navigate,
                  }) => {
    return (
        <View style={styles.HomeCard}>
            <Text style={styles.HomeCardLabel}>{label}</Text>
            <Text style={styles.HomeCardSubLabel}>{subLabel}</Text>
            <View style={styles.CardContainer}>
                {data.map((a, i) => <Card {...a} navigate={navigate} key={i}/>)}
            </View>
        </View>
    );
};

HomeCard.propTypes = {
    label: PropTypes.string.isRequired,
    subLabel: PropTypes.string,
    data: PropTypes.array.isRequired,
};

export default HomeCard;
