import React from 'react';
import PropTypes from 'prop-types';
import {Image, Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";

const Card = ({
                  label = "",
                  image = "",
                  data = [],
                  navigate,
              }) => {

    return (
        <TouchableOpacity onPress={() => navigate(data)}>
            <View style={styles.Card}>
                <Image source={image} style={styles.CardImage}/>
                <Text style={styles.CardLabel}>{label}</Text>
            </View>
        </TouchableOpacity>
    );
};

Card.propTypes = {
    label: PropTypes.string.isRequired,
    image: PropTypes.any.isRequired,
};

export default Card;
