import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color, TOP_PADDING} from "../../config/variables";
import utils from "../../config/utils";

const styles = StyleSheet.create({
    ...CommonStyles,
    AddressCard: (remove) => {
        return {
            borderWidth: 1,
            borderColor: Color.grey5,
            paddingTop: 20,
            paddingLeft: 20,
            paddingRight: 20,
            paddingBottom: remove ? 0 : 20,
            margin: 10,
            borderRadius: 10,
        }
    },
    name: {
        fontSize: 18,
        lineHeight: 25,
        fontWeight: '600',
    },
    mobile: {
        fontSize: 15,
        fontWeight: '600',
        lineHeight: 22,
    },
    address: {
        fontSize: 14,
        lineHeight: 22,
    },
});


export default styles
