import React from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity} from "react-native";
import styles from "./styles";
import CustomButton from "../CustomButton";
import {Color} from "../../config/variables";

const AddressCard = ({
                         _id = "",
                         name = "",
                         mobile = "",
                         building = "",
                         locality = "",
                         landmark = "",
                         removeButton = true,
                         onPress = () => console.log(""),
                         onRemove = () => console.log("")
                     }) => {
    console.log(_id, name, mobile, building, locality, landmark);
    return (
        <TouchableOpacity onPress={onPress} style={styles.AddressCard(removeButton)}>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.address}>{building}</Text>
            <Text style={styles.address}>{locality}</Text>
            <Text style={styles.address}>{landmark}</Text>
            <Text style={styles.mobile}>{mobile}</Text>
            {removeButton && <CustomButton icon={'trash'} type={'clean'} color={Color.red} size={'small'}
                                           onPress={() => onRemove(_id)}>Remove</CustomButton>}
        </TouchableOpacity>
    );
};

AddressCard.prototype = {
    _id: PropTypes.string,
    name: PropTypes.string,
    mobile: PropTypes.number,
    building: PropTypes.string,
    locality: PropTypes.string,
    landmark: PropTypes.string,
    onPress: PropTypes.func,
    removeButton: PropTypes.bool,
    onRemove: PropTypes.func,
};
export default AddressCard;
