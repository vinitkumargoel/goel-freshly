import React from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity} from "react-native";
import {FontAwesome5} from "@expo/vector-icons";
import styles from "./styles";
import {ICON_COLOR} from "../../containers/ProductDetails/helper";

const CustomButton = ({type = "green", icon, onPress, size = 'large', color = "", ...props}) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={{...styles[`CustomButton_${type}`], ...styles.CustomButton(size), ...props.style}}>

            <Text style={{...styles[`CustomButtonText_${type}`](color), ...styles.CustomButtonText(size)}}>
                {icon &&
                <FontAwesome5
                    iconStyle={{...styles[`CustomButtonIcon_${type}`], ...styles.CustomButtonIcon(size)}}
                    size={20}
                    color={ICON_COLOR(type, color)}
                    name={icon}/>}
                <Text> {props.children}</Text>
            </Text>
        </TouchableOpacity>
    );
};

CustomButton.propTypes = {
    color: PropTypes.string,
    size: PropTypes.string,
    icon: PropTypes.string,
    type: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
};

export default CustomButton;
