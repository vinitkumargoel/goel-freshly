import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {ICON_BG, ICON_COLOR} from "../../containers/ProductDetails/helper";

const styles = StyleSheet.create({
    ...CommonStyles,
    //MAIN DESIGNS
    CustomButton: (size) => {
        return {
            margin: 10,
            padding: 10,
            borderRadius: size === "small" ? 12 : 12,
            // flexDirection: 'row',
            paddingVertical: size === "small" ? 10 : 20,
            paddingHorizontal: size === "small" ? 30 : 30,
        }
    },
    CustomButtonText: (size) => {
        return {
            width: '100%',
            alignContent: 'center',
            alignSelf: 'center',
            textAlign: 'center',
            marginLeft: size === "small" ? 10 : 10,
            marginRight: size === "small" ? 10 : 10,
            fontSize: size === "small" ? 18 : 18,
        }
    },
    CustomButtonIcon: (size) => {
        return {
            // width: 122,
            // paddingVertical: 23,
            // paddingRight: 10,
        }
    },

    // Green Button
    CustomButton_green: {
        backgroundColor: ICON_BG('green'),
    },
    CustomButtonText_green: () => {
        return {
            color: ICON_COLOR('green'),
            fontWeight: '600',
        }
    },
    CustomButtonIcon_green: {},

    //CLear Button
    CustomButton_clear: {
        borderWidth: 1,
        backgroundColor: ICON_BG('clear'),
        borderColor: ICON_COLOR('clear'),
    },
    CustomButtonText_clear: () => {
        return {
            color: ICON_COLOR('clear'),
            fontWeight: '600',
        }
    },
    CustomButtonIcon_clear: {},

    //Red Button
    CustomButton_red: {
        backgroundColor: ICON_BG('red'),
    },
    CustomButtonText_red: () => {
        return {
            color: ICON_COLOR('red'),
            fontWeight: '600',
        }
    },
    CustomButtonIcon_red: {},

    //Red Button
    CustomButton_clean: {
        backgroundColor: ICON_BG('clean'),
    },
    CustomButtonText_clean: color => {
        return {
            color: color ? color : ICON_COLOR('clean'),
            fontWeight: '600',
        }
    },
    CustomButtonIcon_clean: {},
});


export default styles
