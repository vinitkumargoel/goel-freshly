import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Text, TextInput, View} from "react-native";
import styles from "./styles";

const CustomTextBox = props => {
    let [focus, setFocus] = useState(false);
    const onFocus = () => {
        setFocus(true);
    };
    const onEndEditing = () => {
        setFocus(false);
    };
    const onChangeText = text => {
        props.onChangeText(text);
    };
    return (
        <View styles={{...styles.CustomTextBox, ...props.styles}}>
            {props.label && <Text style={styles.inputBoxLabel}>{props.label}</Text>}
            <TextInput
                editable={props.editable || true}
                maxLength={props.maxLength || 100}
                keyboardType={props.keyboardType || 'default'}
                secureTextEntry={props.secureTextEntry || false}
                autoCapitalize={props.autoCapitalize}
                style={{...(focus ? styles.inputBoxFocus(props.error || props.subText) : styles.inputBox(props.error || props.subText))}}
                placeholder={props.placeholder}
                onFocus={onFocus}
                onEndEditing={onEndEditing}
                value={props.value}
                onChangeText={onChangeText}
            />
            {props.subText && <Text style={styles.inputBoxSubText}>{props.subText}</Text>}
            {props.error && <Text style={styles.inputBoxError}>{props.errorMessage}</Text>}
        </View>
    );
};

CustomTextBox.propTypes = {
    subText: PropTypes.string,
    error: PropTypes.bool,
    errorMessage: PropTypes.string,
    label: PropTypes.string,
    editable: PropTypes.bool,
    maxLength: PropTypes.number,
    keyboardType: PropTypes.string,
    autoCapitalize: PropTypes.string,
    styles: PropTypes.object,
    placeholder: PropTypes.string,
    onFocus: PropTypes.func,
    secureTextEntry: PropTypes.bool,
    value: PropTypes.string,
    onChangeText: PropTypes.func,
};

export default CustomTextBox;
