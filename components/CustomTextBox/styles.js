import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color} from "../../config/variables";

const styles = StyleSheet.create({
    ...CommonStyles,
    CustomTextBox: {
        width: '100%'
    },
    inputBox: (error) => {
        return {
            fontSize: 20,
            paddingBottom: 10,
            borderBottomColor: "#d4d4d4",
            borderBottomWidth: 1,
            marginBottom: error ? 10 : 30,
            marginTop: 10,
        }
    },
    inputBoxFocus: (error) => {
        return {
            fontSize: 20,
            paddingBottom: 10,
            borderBottomColor: Color.black,
            borderBottomWidth: 1,
            marginBottom: error ? 10 : 30,
            marginTop: 10,
        }
    },
    inputBoxLabel: {
        fontWeight: '400',
    },
    inputBoxError: {
        marginBottom: 40,
        color: Color.red,
    },
    inputBoxSubText: {
        fontSize: 12,
        color: Color.grey1,
        marginBottom: 30,
    }
});


export default styles
