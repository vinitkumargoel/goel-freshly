import React from 'react';
import PropTypes from 'prop-types';
import {Image, Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import momentTz from 'moment-timezone/builds/moment-timezone-with-data';
import {getStatusMessage} from "./helper";

const OrderTile = props => {
    let product = props.products;
    return (
        <TouchableOpacity onPress={props.onPress} style={styles.OrderTile}>
            <View style={styles.ImageContainer}>
                <Image style={styles.Image} source={{uri: product[0].product.image[0]}}/>
            </View>
            <View style={styles.DataContainer}>
                <Text style={styles.Title}>{product[0].product.title}</Text>
                <Text style={styles.Quantity}>Quantity: {product.length}</Text>
                <Text
                    style={styles.Date}>{momentTz(props.status.date).utcOffset("+05:30").format("DD/MMM/YYYY HH:mm")}</Text>
                {getStatusMessage(props.status)}

            </View>
        </TouchableOpacity>
    );
};

OrderTile.propTypes = {};

export default OrderTile;
