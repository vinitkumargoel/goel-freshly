import styles from "./styles";
import {Text} from "react-native";
import React from "react";

export const getStatusMessage = status => {
    switch (status.type) {
        case "CONFIRMED":
            return <React.Fragment>
                <Text style={{...styles.status, ...styles.confirm}}>Amount Paid</Text>
                <Text style={{...styles.status, ...styles.confirm}}>Delivery agent will call you soon!</Text>
            </React.Fragment>;
        case "PREPARING":
            return <React.Fragment>
                <Text style={{...styles.status, ...styles.preparing}}>Order Preparing</Text>
                <Text
                    style={{...styles.status, ...styles.preparing}}>{status.message || `Order will deliver soon!`}</Text>
            </React.Fragment>;
        case "DELIVERED":
            return <React.Fragment>
                <Text style={{...styles.status, ...styles.delivered}}>Order Delivered</Text>
            </React.Fragment>;
        case "CANCELLED":
            return <React.Fragment>
                <Text style={{...styles.status, ...styles.cancelled}}>Order Canceled</Text>
            </React.Fragment>;
    }
};
