import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color, TOP_PADDING} from "../../config/variables";
import utils from "../../config/utils";

const styles = StyleSheet.create({
    ...CommonStyles,
    OrderTile: {
        flexDirection: 'row',
        padding: 10,
        borderWidth: 1,
        borderColor: "#ececec",
        borderRadius: 10,
        marginBottom: 20,
    },
    ImageContainer: {
        width: '30%',
    },
    Image: {
        width: 100,
        height: 120,
        resizeMode: 'stretch',
    },
    DataContainer: {
        width: '70%',
    },
    Title: {
        fontWeight: '600',
    },
    Quantity: {
        marginTop: 5,
        fontSize: 14,
    },
    Date: {
        marginTop: 5,
        fontSize: 14,
        marginBottom: 5,
    },
    status: {
        marginTop: 2,
        fontSize: 15,
        fontWeight: '600',
    },
    confirm: {
        color: Color.orange,
    },
    preparing: {
        color: Color.blue,
    },
    delivered: {
        color: Color.green,
    },
    cancelled: {
        color: Color.red,
    },
});


export default styles
