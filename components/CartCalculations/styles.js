import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import Colors from "../../constants/Colors";

const styles = StyleSheet.create({
    ...CommonStyles,
    CartCalculations: {
        marginHorizontal: 5,
    },
    LabelValue: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingBottom: 15,
    },
    label: {
        fontSize: 18,
        width: '50%',
        color: Colors.cartCalLabel,
    },
    labelBig: {
        fontSize: 25,
        color: Colors.cartCalLabelBig,
    },
    value: {
        width: '50%',
        fontSize: 18,
        textAlign: 'right',
        color: Colors.cartCalValue,
    },
    valueBig: {
        fontSize: 25,
        color: Colors.cartCalLabelBig,
    },
    valueFocused: {
        color: Colors.cartCalValueFocused,
    },
    hrLine: {
        borderBottomColor: Colors.cartCalLabel,
        borderBottomWidth: 1,
        marginHorizontal: 10,
        marginVertical: 10,
    }
});


export default styles
