import React from 'react';
import {Text, View} from "react-native";
import styles from "./styles";
import utils from "../../config/utils";
import {DELIVERY_PRICE, RUPEE_SYMBOL} from "../../utils/config";
import calculateCartValue from "../../utils/calculateCartValue";

const CartCalculations = ({data = []}) => {
    let {subTotal, discount, delivery, totalPayable} = calculateCartValue(data);
    return (
        <View style={styles.CartCalculations}>
            <View style={styles.LabelValue}>
                <Text style={styles.label}>Sub Total</Text>
                <Text style={styles.value}>{RUPEE_SYMBOL}{subTotal}</Text>
            </View>
            <View style={styles.LabelValue}>
                <Text style={styles.label}>Discount</Text>
                <Text style={{...styles.value, ...styles.valueFocused}}>- {RUPEE_SYMBOL}{discount}</Text>
            </View>
            <View style={styles.LabelValue}>
                <Text style={styles.label}>Delivery</Text>
                <Text style={styles.value}>{RUPEE_SYMBOL}{delivery}</Text>
            </View>
            <View style={styles.hrLine}/>
            <View style={styles.LabelValue}>
                <Text style={{...styles.label, ...styles.labelBig}}>Total Payable</Text>
                <Text style={{...styles.value, ...styles.valueBig}}>{RUPEE_SYMBOL}{totalPayable}</Text>
            </View>
        </View>
    );
};

CartCalculations.propTypes = {};

export default CartCalculations;
