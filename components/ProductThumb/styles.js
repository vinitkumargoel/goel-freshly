import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color} from "../../config/variables";
import utils from "../../config/utils";

const styles = StyleSheet.create({
    ...CommonStyles,
    ProductThumb: {
        width: utils.columnSize * 12,
        paddingHorizontal: 7,
    },
    Poster: {
        width: (utils.columnSize * 11) - 10,
        height: 160,
        resizeMode: 'stretch',
        marginBottom: 10,
    },
    Title: {
        fontSize: 13,
        fontWeight: "bold",
    },
    PriceContainer: {
        flexDirection: 'row',
    },
    Price: sale => {
        return {
            textDecorationLine: sale ? 'line-through' : 'none',
            color: Color.darkRed,
            marginBottom: 20,
            fontSize: 15,
            fontWeight: "bold",
        }
    },
    Sale: {
        marginLeft: 10,
        fontSize: 14,
        lineHeight: 20,
        color: Color.darkRed,
        fontWeight: "bold",
    },
    Crossed: {},
});


export default styles
