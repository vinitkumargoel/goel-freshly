import React from 'react';
import PropTypes from 'prop-types';
import {Image, Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import {useNavigation} from '@react-navigation/native';
import utils from "../../config/utils";
import {RUPEE_SYMBOL} from "../../utils/config";
import CustomButton from "../CustomButton";

const ProductThumb = ({title = "", image = [], price = {regular: 0, sale: 0}, _id = ""}) => {
    let isSale = utils.isSaleProduct(price);

    let navigation = useNavigation();
    return (
        <TouchableOpacity onPress={() => navigation.navigate('product_details', {id: _id})}>
            <View style={styles.ProductThumb}>
                <Image source={{uri: image[0]}} style={styles.Poster}/>
                <Text style={styles.Title}>{title}</Text>
                <View style={styles.PriceContainer}>
                    <Text style={styles.Price(isSale)}>{RUPEE_SYMBOL}{price.regular}</Text>
                    {isSale && <Text style={styles.Sale}>{RUPEE_SYMBOL}{price.sale}</Text>}
                </View>
                {/*<CustomButton type={'green'} size={'small'} onPress={() => console.log("")}>Add Cart</CustomButton>*/}
            </View>

        </TouchableOpacity>
    );
};

ProductThumb.propTypes = {
    title: PropTypes.string.isRequired,
    image: PropTypes.array.isRequired,
    price: PropTypes.object.isRequired,
};

export default ProductThumb;
