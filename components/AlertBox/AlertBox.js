import {Alert} from "react-native";

const AlertBox = message => {
    return Alert.alert(
        'Error', message,
        [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
    );
};

export default AlertBox;
