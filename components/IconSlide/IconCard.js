import React from 'react';
import PropTypes from 'prop-types';
import {Image, Text, TouchableOpacity} from "react-native";
import styles from "./styles";

const IconCard = ({
                      label = "",
                      image = "",
                      data = [],
                      navigate
                  }) => {
    return (
        <TouchableOpacity onPress={() => navigate(data)} style={styles.IconCard}>
            <Image source={image} style={styles.IconCardImage}/>
            <Text style={styles.IconCardLabel}>{label}</Text>
        </TouchableOpacity>
    );
};

IconCard.propTypes = {
    label: PropTypes.string.isRequired,
    image: PropTypes.any.isRequired,
};

export default IconCard;
