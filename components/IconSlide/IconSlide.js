import React from 'react';
import PropTypes from 'prop-types';
import {ScrollView, Text, View} from "react-native";
import styles from "./styles";
import IconCard from "./IconCard";

const IconSlide = ({
                       label = "",
                       data = [],
                       navigate
                   }) => {
    return (
        <View style={styles.IconSlide}>
            <Text style={styles.label}>{label}</Text>
            <ScrollView
                style={styles.IconCardContainer}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            >
                {data.map((a, i) => <IconCard navigate={navigate} {...a} key={i}/>)}
            </ScrollView>
        </View>
    );
};

IconSlide.propTypes = {
    data: PropTypes.array.isRequired,
    label: PropTypes.string.isRequired,
};

export default IconSlide;
