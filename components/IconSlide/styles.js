import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";

const styles = StyleSheet.create({
    ...CommonStyles,
    IconSlide: {
        marginBottom: 20,
    },
    label: {
        fontSize: 20,
        fontWeight: "bold",
        paddingBottom: 10,
    },
    IconCardContainer: {
        flexDirection: 'row',
    },
    IconCard: {
        paddingVertical: 10,
        paddingRight: 35,
    },
    IconCardLabel: {
        paddingTop: 5,
        fontSize: 12,
        fontWeight: "400",
    },
    IconCardImage: {
        width: 70,
        height: 70,
        resizeMode: 'stretch',
    }
});


export default styles
