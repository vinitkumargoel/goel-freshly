import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color} from "../../config/variables";

const styles = StyleSheet.create({
    ...CommonStyles,
    VerificationInput: {
        flex: 1,
        padding: 20,
    },
    codeFiledRoot: {
        marginTop: 10,
    },
    cell: {
        paddingTop: 10,
        width: 60,
        marginVertical: 10,
        height: 60,
        lineHeight: 38,
        fontSize: 30,
        borderWidth: 1,
        borderColor: Color.grey5,
        textAlign: 'center',
    },
    focusCell: {
        borderColor: Color.black,
    },
});


export default styles
