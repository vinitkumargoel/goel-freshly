import React from 'react';
import PropTypes from 'prop-types';
import {Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import {FontAwesome5} from "@expo/vector-icons";
import {Color} from "../../config/variables";
import {CONTACT_US_MOBILE} from "../../utils/config";

const OrderFailed = props => {
    return (
        <TouchableOpacity onPress={props.onPress} style={styles.OrderFailed}>
            <View style={styles.iconContainer}>
                <FontAwesome5 size={60} name={'times-circle'} color={Color.white}/>
            </View>
            <View style={styles.messageContainer}>
                <Text style={styles.FailedLabel}>Order Failed</Text>
                <Text style={styles.FailedMessage}>{props.data.description}</Text>
                <Text style={styles.FailedMessage}>Please Try again! If you have any problem please call {CONTACT_US_MOBILE}</Text>
            </View>
        </TouchableOpacity>
    );
};


export default OrderFailed;
