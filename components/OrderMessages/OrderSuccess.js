import React from 'react';
import PropTypes from 'prop-types';
import {Image, Text, View} from "react-native";
import styles from "./styles";
import CustomButton from "../CustomButton";
import {RUPEE_SYMBOL} from "../../utils/config";

const OrderSuccess = ({navigation, route, ...props}) => {
    console.log();
    return (
        <View style={styles.OrderSuccess}>
            <View style={styles.IconContainer}>
                <Image style={styles.SuccessIcon} source={require("../../assets/images/order-success.png")}/>
            </View>
            <Text style={styles.OrderSuccessText}>Order is Successful!!</Text>
            <Text style={styles.OrderSuccessText}>Amount {RUPEE_SYMBOL}{route.params.data.amount} Paid</Text>
            <CustomButton style={styles.dismissButton}
                          type={'green'}
                          onPress={() => navigation.navigate("Home")}>
                Dismiss
            </CustomButton>
        </View>
    );
};

OrderSuccess.propTypes = {};

export default OrderSuccess;
