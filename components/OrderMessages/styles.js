import {StyleSheet} from "react-native";
import CommonStyles from "../../config/common.style";
import {Color, TOP_PADDING} from "../../config/variables";
import utils from "../../config/utils";

const styles = StyleSheet.create({
    ...CommonStyles,
    OrderFailed: {
        marginTop: 20,
        marginHorizontal: 10,
        backgroundColor: Color.red,
        padding: 20,
        borderRadius: 10,
        color: Color.white,
        flexDirection: 'row',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 7,
    },
    iconContainer: {
        width: '30%',
    },
    messageContainer: {
        width: '70%',
    },
    FailedLabel: {
        color: Color.white,
        fontSize: 18,
        fontWeight: '700',
        marginBottom: 7,
    },
    FailedMessage: {
        color: Color.white,
        fontSize: 15,
        fontWeight: '500',
        marginBottom: 5,
    },
    OrderSuccess: {
        marginTop: 20,
        padding: 20,
    },
    SuccessIcon: {
        marginTop: 150,
        width: 200,
        height: 200,
        alignItems: 'center',
        alignSelf: 'center',
        alignContent: 'center',
        resizeMode: 'stretch',
    },
    IconContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
    },
    OrderSuccessText: {
        fontSize: 20,
        lineHeight: 28,
        fontWeight: '500',
        alignItems: 'center',
        textAlign: 'center',
    },
    dismissButton: {
        marginTop: 50,
    }
});


export default styles
